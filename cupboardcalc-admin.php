<?php

/* Styles and scripts */

function cupboard_admin_styles() {
	wp_register_style( 'jquery-ui', plugins_url( '/css/jquery-ui.css', __FILE__ ), array(), '1', 'all' );
	wp_register_style( 'cupboard-style', plugins_url( '/css/cupboard-style-admin.css', __FILE__ ), array(), '1', 'all' );
	
	wp_enqueue_style( 'cupboard-style' );	
	wp_enqueue_style( 'jquery-ui' );

}

function cupboard_admin_script() {
	wp_register_script( 'cupboard-script', plugins_url( '/js/cupboard-script-admin.js', __FILE__ ), array('jquery'), '1.0', true );
	
	wp_enqueue_script("jquery-ui-core", array('jquery'));
	wp_enqueue_script("jquery-ui-draggable", array('jquery','jquery-ui-core'));
	wp_enqueue_script("jquery-ui-resizable", array('jquery','jquery-ui-core'));
	
	//media gallery
	wp_enqueue_media();
	wp_enqueue_script( 'custom-header' );
		
	wp_enqueue_script('cupboard-script');
	
	wp_localize_script( 'cupboard-script', 'pluginURL', array('plugin_dir' => plugins_url( '' , __FILE__ )));
		
}

function redirect($page, $tab) {
	
	echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page='.$page.'&tab='.$page.'_'.$tab.'&updated=true">';
	
	exit;
	add_action('admin_notices', 'my_admin_notice');
}

//Additional functions

function translit($st) {
    $lit = array(
        "а" => "a","б" => "b","в" => "v","г" => "g","д" => "d","е" => "e","ё" => "yo","ж" => "g","з" => "z","и" => "i","й" => "y","к" => "k","л" => "l","м" => "m",
        "н" => "n","о" => "o","п" => "p","р" => "r","с" => "s","т" => "t","у" => "u","ф" => "f","х" => "h","ц" => "ts","ч" => "ch","ш" => "sh","щ" => "shch","ъ" => "",
        "ы" => "i","ь" => "","э" => "e","ю" => "yu","я" => "ya",

        "А" => "A","Б" => "B","В" => "V","Г" => "G","Д" => "D","Е" => "E","Ё" => "Yo","Ж" => "G","З" => "Z","И" => "I","Й" => "Y","К" => "K","Л" => "L","М" => "M",
        "Н" => "N","О" => "O","П" => "P","Р" => "R","С" => "S","Т" => "T","У" => "U","Ф" => "F","Х" => "H","Ц" => "Ts","Ч" => "Ch","Ш" => "Sh","Щ" => "Shch","Ъ" => "",
        "Ы" => "I","Ь" => "","Э" => "E","Ю" => "Yu","Я" => "Ya",);
    return $st = strtr($st, $lit);
}

function url2path($url){
    $url=str_replace(rtrim(get_site_url(),'/').'/', ABSPATH, $url);
    return $url;
    }

function multiple_inputs($input_name, $array = 0, $inner_arrays = false) {
	$inputs_array = array();

	if(count($_POST[$input_name]) > 0) {
			foreach($_POST[$input_name] as $key=>$value)
				$inner_arrays === false ? $inputs_array[] = $value : $inputs_array[] = '['.$value.']' ;
			}
	
			
	$array == 0 ? $return = implode(", ", $inputs_array) : $return = $inputs_array;
	
	return $return;
}

function cupboard_add_admin_page() {
	add_menu_page('Калькулятор Шкафов', 'Калькулятор Шкафов', 1, 'cupboard', 'cupboard_options_page', '', 61);
	add_submenu_page( 'cupboard', 'Корпуса', 'Корпуса', 1, 'corpus', 'corpus_options_page' );
	add_submenu_page( 'cupboard', 'Ламинат', 'Ламинат', 1, 'laminate', 'laminate_options_page' );
	add_submenu_page( 'cupboard', 'Профиль', 'Профиль', 1, 'profile', 'profile_options_page' );
	add_submenu_page( 'cupboard', 'Рамки', 'Рамки', 1, 'frame', 'frame_options_page' );
	add_submenu_page( 'cupboard', 'Заполнения', 'Заполнения', 1, 'image', 'image_options_page' );
	add_submenu_page( 'cupboard', 'Угловые сегменты', 'Угловые сегменты', 1, 'corner', 'corner_options_page' );
	add_submenu_page( 'cupboard', 'Дополнительные элементы', 'Дополнительные элементы', 1, 'additional', 'additional_options_page' );
	
	add_action('admin_init', 'cupboard_admin_script');
	add_action('admin_print_styles', 'cupboard_admin_styles');
	if ( isset ( $_GET['updated'] ) === 'true' ) add_action('admin_notices', 'my_admin_notice');
	
}

function cupboard_options_page() {
	
	echo "<h2>Калькулятор шкафов</h2>";
	
	add_option('cupboard_number','0');
	add_option('cupboard_order_mail','name@example.com');
	add_option('cupboard_cities_city','');
	add_option('cupboard_cities_multi','');
	
	echo "<p>Базовая страница, на которой отображены основные настройки плагина</p>";
	
	if (isset($_POST['cupboard_base_setup_btn'])) 
	{   
	   if ( function_exists('current_user_can') && 
			!current_user_can('edit_others_posts') )
				die ( _e('Hacker?', 'cupboard') );

		if (function_exists ('check_admin_referer') )
		{
			check_admin_referer('cupboard_base_setup_form');
		}

		$cupboard_discount_enable = $_POST['cupboard_discount_enable'];
		$cupboard_discount_value = $_POST['cupboard_discount_value'];
		$cupboard_discount_reason = $_POST['cupboard_discount_reason'];
		$cupboard_user_delegate = $_POST['cupboard_user_delegate'];
		$cupboard_order_mail = $_POST['cupboard_order_mail'];
		$cupboard_cities_city = multiple_inputs('cupboard_cities_city');
		$cupboard_cities_multi = multiple_inputs('cupboard_cities_multi');
		
		update_option('cupboard_discount_enable', $cupboard_discount_enable);
		update_option('cupboard_discount_value', $cupboard_discount_value);
		update_option('cupboard_discount_reason', $cupboard_discount_reason);
		update_option('cupboard_user_delegate', $cupboard_user_delegate);
		update_option('cupboard_order_mail', $cupboard_order_mail);
		update_option('cupboard_cities_city', $cupboard_cities_city);
		update_option('cupboard_cities_multi', $cupboard_cities_multi);
		
		redirect('cupboard', '');

	}
	
	//Обнуление базы
	if ( isset($_POST['cupboard_base_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('cupboard_base_setup_form');
        }

		cupboardcalc_uninstall();
		cupboardcalc_install();
		
    }
	//Форма информации о магазине
	echo 
	"
		<form class='base_form' name='cupboard_base_setup' method='post' action='".$_SERVER['PHP_SELF']."?page=cupboard&amp;updated=true'>
	";
	
	if (function_exists ('wp_nonce_field') )
	{
		wp_nonce_field('cupboard_base_setup_form'); 
	}
	
	$cupboard_cities_city = explode(", ", get_option('cupboard_cities_city'));
	$cupboard_cities_multi = explode(", ", get_option('cupboard_cities_multi'));
	
	echo
	"
		<table>
			<tr>
				<td style='text-align:right;'><h2>Режим скидки:</h2></td>
			</tr>
			<tr>
				<td></td>
				<td style='text-align:right;'><label>Включить режим скидки:</label></td>
				<td><input type='checkbox' name='cupboard_discount_enable' value='1'";
				if (get_option('cupboard_discount_enable') == '1') echo 'checked';
					echo "/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td style='text-align:right;'>Размер скидки: </td>
				<td><input type='text' style='width: 30px' name='cupboard_discount_value' value='".get_option('cupboard_discount_value')."'/> %</td>
			</tr>
			<tr>
				<td></td>
				<td style='text-align:right;'>Причина скидки: </td>
				<td><input type='text' style='width: 300px' name='cupboard_discount_reason' value='".get_option('cupboard_discount_reason')."'/></td>
			</tr>
			<tr>
				<td style='text-align:right;'><h2>Права доступа:</h2></td>
			</tr>
			<tr>
				<td></td>
				<td style='text-align:right;'><label>Введите username:</label></td>
				<td><input type='text' style='width: 130px' name='cupboard_user_delegate' value='".get_option('cupboard_user_delegate')."'/> <i style='color: #666'>Права не ниже Редактора!</i></td>
			</tr>
			<tr>
				<td style='text-align:right;'><h2>E-mail для приема заказов:</h2></td>
			</tr>
			<tr>
				<td></td>
				<td style='text-align:right;'><label>Введите e-mail:</label></td>
				<td><input type='text' style='width: 300px' name='cupboard_order_mail' value='".get_option('cupboard_order_mail')."'/></td>
			</tr>
			<tr>
				<td style='text-align:right;'><h2>Множитель цен для городов</h2></td>
				<td><input type='button' class='button button_city_add' value='Добавить город' /></td>
			</tr>";
				
				for($i = 0; $i < count($cupboard_cities_city); $i++) {
						echo "

						<tr>
							<td></td>
							<td style='text-align:right;'><label>Название:</label></td>
							<td>
								<input type='text' style='width: 200px' name='cupboard_cities_city[]' value='".$cupboard_cities_city[$i]."'/>
								<label>Множитель:</label>
								<input type='text' style='width: 100px' name='cupboard_cities_multi[]' value='".$cupboard_cities_multi[$i]."'/>
								<a class='button_city_delete'>X</a>
							</td>
						</tr>
						
						";
					}
		echo "<tr>
				<td>&nbsp;</td>
				<td style='text-align:center'>
					<input type='submit' class='button' name='cupboard_base_setup_btn' value='Сохранить' style=''/>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style='text-align:center'>
				<!--	<input type='submit' class='button' name='cupboard_base_delete_btn' value='Обнулить базу данных' /> -->
				</td>
				<td>&nbsp;</td>
			</tr>
			
				
		</table>
	</form>
	";
	
}
	
	



/*
	КОРПУСА!
*/

function corpus_options_page() {
	if ( $_GET['page'] == 'corpus' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=corpus&tab=corpus_list">';
		exit;
	}
	
	echo "<h2>Каталог корпусов</h2><br />";
	
	$tab_pages = array( 'corpus_list' => 'Список моделей корпуса', 'corpus_add' => 'Добавить корпус');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'corpus'); else cupboard_admin_tabs($tab_pages, 'corpus_list', 'corpus');

	if ( $_GET['tab'] == 'corpus_list' && $_GET['page'] == 'corpus' ){
		corpus_list_product();
	} else if ( $_GET['tab'] == 'corpus_add' && $_GET['page'] == 'corpus' ) {
		corpus_add_product();
	}

}

function corpus_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corpuses;
	
	//Сохранение изменений товаров
	if ( isset($_POST['corpus_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corpus_list_setup_form');
        }
		  
		//Файлы
		if ( isset ($_FILES['corpus_foto']) && $_FILES['corpus_foto']['name'] != '') {
			$file_name_new = translit($_FILES['corpus_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/", 0755, true);
			}
			
			copy ( $_FILES['corpus_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 

		}
		
		$corpus_list_id = $_POST['corpus_list_id'];
		
		//проверка фото, в случае несовпадения переписывает
		$corpus_photo_url_now = $wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$corpus_list_id );
		$corpus_photo_url = '/wp-content/uploads/cupboardcalc/corpuses/'.$file_name_new;
		$file_name_new != null ? $corpus_photo_url = $corpus_photo_url : $corpus_photo_url = $corpus_photo_url_now;
		 
		$corpus_name = stripslashes_deep($_POST['corpus_name']);
		$corpus_doors = multiple_inputs('corpus_doors');
		$corpus_width = multiple_inputs('corpus_width');
		$corpus_height = multiple_inputs('corpus_height');
		$corpus_depth = multiple_inputs('corpus_depth');
		$corpus_table_sizes = multiple_inputs('corpus_table_sizes');
		$corpus_table_pricesBase = multiple_inputs('corpus_table_pricesBase');
		$corpus_table_pricesAnother = multiple_inputs('corpus_table_pricesAnother');
		$corpus_table_articul = multiple_inputs('corpus_table_articul');
		$corpus_table_doors = multiple_inputs('corpus_table_doors', 0, true);
		
		
			if (!empty($corpus_photo_url) && !empty($corpus_name) && !empty($corpus_doors) && $corpus_width !=', , ' && $corpus_height !=', , ' && $corpus_depth !=', , ') {		
			if (!empty($corpus_table_sizes)) {
		
			
			
			$wpdb->update
					(
						$table_products,  
						array( 'name' => $corpus_name, 'photo_url' => $corpus_photo_url, 'corpus_doors' => $corpus_doors,
							   'corpus_width' => $corpus_width, 'corpus_height' => $corpus_height, 'corpus_depth' => $corpus_depth,
							   'corpus_table_sizes' => $corpus_table_sizes, 'corpus_table_pricesBase' => $corpus_table_pricesBase,
							   'corpus_table_pricesAnother' => $corpus_table_pricesAnother, 'corpus_table_articul' => $corpus_table_articul,
							   'corpus_table_doors' => $corpus_table_doors),
						array( 'id' => $corpus_list_id ), 
						array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'),
						array( '%d' )
					);
			}
		}
		
		redirect('corpus', 'list');
			
    }
	
	//Удаление товара
	if ( isset($_POST['corpus_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corpus_list_setup_form');
        }

        $corpus_list_id = $_POST['corpus_list_id'];

		$corpus_photo_url_now = url2path(home_url().($wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$corpus_list_id )));
		unlink($corpus_photo_url_now);

		$wpdb->query("DELETE FROM $table_products WHERE id = $corpus_list_id");
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		
		$corpus_doors_base = explode(", ", $item->corpus_doors);
		$corpus_height_base = explode(", ", $item->corpus_height);
		$corpus_width_base = explode(", ", $item->corpus_width);
		$corpus_depth_base = explode(", ", $item->corpus_depth);
		
		$corpus_table_sizes = explode(", ", $item->corpus_table_sizes);
		$corpus_table_pricesBase = explode(", ", $item->corpus_table_pricesBase);
		$corpus_table_pricesAnother = explode(", ", $item->corpus_table_pricesAnother);
		$corpus_table_articul = explode(", ", $item->corpus_table_articul);
		$corpus_table_doors = explode(", ", $item->corpus_table_doors);
		
		$corpus_photo_url_now = $item->photo_url;
		
		echo
		"
			<form id='corpus_list' class='list_form' name='corpus_list_setup' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=corpus&amp;tab=corpus_list&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('corpus_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='corpus_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='corpus_name' value='".$item->name."' style='width:300px;'/></td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Фото:</td>
					<td><img src='".home_url().$item->photo_url."'/></td>
					<td style='text-align:right;'>Изменить фото:</td>
					<td><input type='file' name='corpus_foto' accept='image/*' value='".$item->photo_url."' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Количество дверей:</td>
					<td><label> 2 <input type='checkbox' name='corpus_doors[]' value='2'";
					if (in_array('2',$corpus_doors_base)) echo "checked";
					echo "/></label>
					<label> 3 <input type='checkbox' name='corpus_doors[]' value='3'";
					if (in_array('3',$corpus_doors_base)) echo "checked";
					echo "/></label>
					<label> 4 <input type='checkbox' name='corpus_doors[]' value='4'";
					if (in_array('4',$corpus_doors_base)) echo "checked";
					echo "/></label></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Размеры:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td> 
						<label>Ширина - </label>
						<label>Мин <input id='corpus_add_min_width' type='text' name='corpus_width[]' style='width:50px;' value='".$corpus_width_base[0]."' /></label>
						<label>Макс <input id='corpus_add_max_width' type='text' name='corpus_width[]' style='width:50px;' value='".$corpus_width_base[1]."' /></label>
						<label>Шаг <input id='corpus_add_step_width' type='text' name='corpus_width[]' style='width:50px;' value='".$corpus_width_base[2]."' /></label>
						
						<br />
						
						<label>Высота<span style='padding-left:7px;'> </span> </label> -
						<label>Мин <input type='text' name='corpus_height[]' style='width:50px;' value='".$corpus_height_base[0]."' /></label>
						<label>Макс <input type='text' name='corpus_height[]' style='width:50px;' value='".$corpus_height_base[1]."' /></label>
						<label>Шаг <input type='text' name='corpus_height[]' style='width:50px;' value='".$corpus_height_base[2]."' /></label>
						
						<br />
						
						<label>Глубина - </label>
						<label>Мин <input type='text' name='corpus_depth[]' style='width:50px;' value='".$corpus_depth_base[0]."' /></label>
						<label>Макс <input type='text' name='corpus_depth[]' style='width:50px;' value='".$corpus_depth_base[1]."' /></label>
						<label>Шаг <input type='text' name='corpus_depth[]' style='width:50px;' value='".$corpus_depth_base[2]."' /></label>
						
						<br />
						<br />
						
						<button class='corpus_add_size_table_reload'>Обновить Таблицу цен</button>
						
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Таблица Цен<br/>(заполняется на<br /> основании ширины):</td>
					<td>
					<table class='corpus_add_size_table'>
						<tr class='corpus_add_raw_sizes'>
							<td class='first_cell'>Ширина</td>";
					
					for($i = 0; $i < count($corpus_table_sizes); $i++) {
						echo "<td><input type='text' class='corpus_add_input_sizes' name='corpus_table_sizes[]' style='width:50px;' value='".$corpus_table_sizes[$i]."' /></td>";
					}
					
					echo "</tr>
						<tr class='corpus_add_raw_pricesBase'>
							<td class='first_cell'>Цена для базовой высоты</td>";
					
					for($i = 0; $i < count($corpus_table_pricesBase); $i++) {
						echo "<td><input type='text' class='corpus_add_input_pricesBase' name='corpus_table_pricesBase[]' style='width:50px;' value='".$corpus_table_pricesBase[$i]."' /></td>";
					}		
							
					echo "</tr>
						<tr class='corpus_add_raw_pricesAnother'>
							<td class='first_cell'>Цена высоты, отличной от базовой</td>";
					
					for($i = 0; $i < count($corpus_table_pricesAnother); $i++) {
						echo "<td><input type='text' class='corpus_add_input_pricesAnother' name='corpus_table_pricesAnother[]' style='width:50px;' value='".$corpus_table_pricesAnother[$i]."' /></td>";
					}			
							
					echo "</tr>
						<tr class='corpus_add_raw_articul'>
							<td class='first_cell'>Артикул</td>";
					
					for($i = 0; $i < count($corpus_table_articul); $i++) {
						echo "<td><input type='text' class='corpus_add_input_articul' name='corpus_table_articul[]' style='width:50px;' value='".$corpus_table_articul[$i]."' /></td>";
					}	
					
					echo "</tr>
						<tr class='corpus_add_raw_doors'>
							<td class='first_cell'>Количество дверей</td>";

						for($i = 0; $i < count($corpus_table_articul); $i++) {
							$corpus_table_doors[$i] = preg_replace('/[\[(.)\]]/', '', $corpus_table_doors[$i]);
							$doors = explode(",", $corpus_table_doors[$i]);
							echo "<td><label> 2 <input type='checkbox' class='corpus_table_doors' value='2'";
							if (in_array('2',$doors)) echo "checked";
							echo "/></label><br>
							<label> 3 <input type='checkbox' class='corpus_table_doors' value='3'";
							if (in_array('3',$doors)) echo "checked";
							echo "/></label><br>
							<label> 4 <input type='checkbox' class='corpus_table_doors' value='4'";
							if (in_array('4',$doors)) echo "checked";
							echo "/></label><input type='hidden' name='corpus_table_doors[]' value='".implode(",", $doors)."' /></td>";
						}

						echo "</tr>
						
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='corpus_add_btn' type='submit' class='button' name='corpus_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='corpus_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}

function corpus_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corpuses;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['corpus_add_btn']) ) 
    {
    
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corpus_add_form');
        }
        
        //Файлы
		if ( isset ($_FILES['corpus_foto']) && $_FILES['corpus_foto']['name'] != '') {
			$file_name_new = translit($_FILES['corpus_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/corpuses/", 0755, true);
			}
			
			copy ( $_FILES['corpus_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 
		}
		
		  
		$corpus_name = stripslashes_deep($_POST['corpus_name']);
		$corpus_photo_url = '/wp-content/uploads/cupboardcalc/corpuses/'.$file_name_new;
		$corpus_doors = multiple_inputs('corpus_doors');
		$corpus_width = multiple_inputs('corpus_width');
		$corpus_height = multiple_inputs('corpus_height');
		$corpus_depth = multiple_inputs('corpus_depth');
		$corpus_table_sizes = multiple_inputs('corpus_table_sizes');
		$corpus_table_pricesBase = multiple_inputs('corpus_table_pricesBase');
		$corpus_table_pricesAnother = multiple_inputs('corpus_table_pricesAnother');
		$corpus_table_articul = multiple_inputs('corpus_table_articul');
		$corpus_table_doors = multiple_inputs('corpus_table_doors', 0 , true);
		
			if (!empty($corpus_photo_url) && !empty($corpus_name) && !empty($corpus_doors) && $corpus_width !=', , ' && $corpus_height !=', , ' && $corpus_depth !=', , ') {		
			if (!empty($corpus_table_sizes)) {
			$wpdb->insert
					(
						$table_products,  
						array( 'name' => $corpus_name, 'photo_url' => $corpus_photo_url, 'corpus_doors' => $corpus_doors,
							   'corpus_width' => $corpus_width, 'corpus_height' => $corpus_height, 'corpus_depth' => $corpus_depth,
							   'corpus_table_sizes' => $corpus_table_sizes, 'corpus_table_pricesBase' => $corpus_table_pricesBase,
							   'corpus_table_pricesAnother' => $corpus_table_pricesAnother, 'corpus_table_articul' => $corpus_table_articul,
							   'corpus_table_doors' => $corpus_table_doors),

						array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
					);
			}
		}
		
		redirect('corpus', 'add');
    
    }
    
    
	
	//Форма добавления товара
	echo
		"
			
			<form id='corpus_add' class='add_form' name='corpus_add' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=corpus&amp;tab=corpus_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('corpus_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='corpus_name' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Загрузить фото:</td>
					<td><input type='file' name='corpus_foto' accept='image/*' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Количество дверей:</td>
					<td><label> 2 <input type='checkbox' name='corpus_doors[]' value='2'/></label>
					<label> 3 <input type='checkbox' name='corpus_doors[]' value='3' /></label>
					<label> 4 <input type='checkbox' name='corpus_doors[]' value='4' /></label></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Размеры:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td> 
						<label>Ширина - </label>
						<label>Мин <input id='corpus_add_min_width' type='text' name='corpus_width[]' style='width:50px;' value='1000'/></label>
						<label>Макс <input id='corpus_add_max_width' type='text' name='corpus_width[]' style='width:50px;' value='2500'/></label>
						<label>Шаг <input id='corpus_add_step_width' type='text' name='corpus_width[]' style='width:50px;' value='50'/></label>
						
						<br />
						
						<label>Высота<span style='padding-left:7px;'> </span> </label> - 
						<label>Мин <input type='text' name='corpus_height[]' style='width:50px;' value='2000'/></label>
						<label>Макс <input type='text' name='corpus_height[]' style='width:50px;' value='2400'/></label>
						<label>Шаг <input type='text' name='corpus_height[]' style='width:50px;' value='50'/></label>
						
						<br />
						
						<label>Глубина - </label>
						<label>Мин <input type='text' name='corpus_depth[]' style='width:50px;' value='450'/></label>
						<label>Макс <input type='text' name='corpus_depth[]' style='width:50px;' value='600'/></label>
						<label>Шаг <input type='text' name='corpus_depth[]' style='width:50px;' value='150'/></label>
						
						<br />
						
						<br />
						
						<button class='corpus_add_size_table_reload'>Обновить Таблицу цен</button>
						
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Таблица Цен<br/>(заполняется на<br /> основании ширины):</td>
					<td>
					<table class='corpus_add_size_table'>
						<tr class='corpus_add_raw_sizes'>
							<td class='first_cell'>Ширина</td>	
						</tr>
						<tr class='corpus_add_raw_pricesBase'>
							<td class='first_cell'>Цена для базовой высоты</td>
						</tr>
						<tr class='corpus_add_raw_pricesAnother'>
							<td class='first_cell'>Цена высоты, отличной от базовой</td>
						</tr>
						<tr class='corpus_add_raw_articul'>
							<td class='first_cell'>Артикул</td>
						</tr>
						<tr class='corpus_add_raw_doors'>
							<td class='first_cell'>Количество дверей</td>
						</tr>
						
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='corpus_add_btn' type='submit' class='button' name='corpus_add_btn' value='Добавить' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

/*
	ЛАМИНАТ!
*/

function laminate_options_page() {
	if ( $_GET['page'] == 'laminate' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=laminate&tab=laminate_list">';
		exit;
	}
	
	echo "<h2>Каталог Ламината</h2><br />";
	
	$tab_pages = array( 'laminate_list' => 'Список моделей ламината', 'laminate_add' => 'Добавить ламинат');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'laminate'); else cupboard_admin_tabs($tab_pages, 'laminate_list', 'laminate');


	if ( $_GET['tab'] == 'laminate_list' && $_GET['page'] == 'laminate' ){
		laminate_list_product();
	} else if ( $_GET['tab'] == 'laminate_add' && $_GET['page'] == 'laminate' ) {
		laminate_add_product();
	}

}

function laminate_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_laminate;
	
	//Сохранение изменений товаров
	if ( isset($_POST['laminate_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('laminate_list_setup_form');
        }
		  
		//Файлы
		if ( isset ($_FILES['laminate_foto']) && $_FILES['laminate_foto']['name'] != '') {
			$file_name_new = translit($_FILES['laminate_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/", 0755, true);
			}
			
			copy ( $_FILES['laminate_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 

		}
		
		$laminate_list_id = $_POST['laminate_list_id'];
		
		//проверка фото, в случае несовпадения переписывает
		$laminate_photo_url_now = $wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$laminate_list_id );
		$laminate_photo_url = '/wp-content/uploads/cupboardcalc/laminate/'.$file_name_new;
		$file_name_new != null ? $laminate_photo_url = $laminate_photo_url : $laminate_photo_url = $laminate_photo_url_now;
		 
		$laminate_name = stripslashes_deep($_POST['laminate_name']);
				
		
			if (!empty($laminate_photo_url) && !empty($laminate_name)) {		
		
			$wpdb->update
					(
						$table_products,  
						array( 'name' => $laminate_name, 'photo_url' => $laminate_photo_url),
						array( 'id' => $laminate_list_id ), 
						array( '%s', '%s'),
						array( '%d' )
					);
			}
		redirect('laminate', 'list');
    }
	
	//Удаление товара
	if ( isset($_POST['laminate_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('laminate_list_setup_form');
        }
		  
		$laminate_list_id = $_POST['laminate_list_id'];

		$laminate_photo_url_now = url2path(home_url().($wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$laminate_list_id)));
        unlink($laminate_photo_url_now);
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $laminate_list_id"); 
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		
		echo
		"
			<form id='laminate_list' class='list_form' name='laminate_list_setup' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=laminate&amp;tab=laminate_list&amp;updated=true'>
			<h3 class='form_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('laminate_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='laminate_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='laminate_name' value='".$item->name."' style='width:300px;'/></td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Фото:</td>
					<td><img src='".home_url().$item->photo_url."'/></td>
					<td style='text-align:right;'>Изменить фото:</td>
					<td><input type='file' name='laminate_foto' accept='image/*' value='".$item->photo_url."' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='laminate_add_btn'type='submit' class='button' name='laminate_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='laminate_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}

function laminate_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_laminate;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['laminate_add_btn']) ) 
    {
    
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('laminate_add_form');
        }
        
        //Файлы
		if ( isset ($_FILES['laminate_foto']) && $_FILES['laminate_foto']['name'] != '') {
			$file_name_new = translit($_FILES['laminate_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/laminate/", 0755, true);
			}
			
			copy ( $_FILES['laminate_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 
		}
		
		  
		$laminate_name = stripslashes_deep($_POST['laminate_name']);
		$laminate_photo_url = '/wp-content/uploads/cupboardcalc/laminate/'.$file_name_new;
		
			if (!empty($laminate_photo_url) && !empty($laminate_name)) {		
			$wpdb->insert
					(
						$table_products,  
						array( 'name' => $laminate_name, 'photo_url' => $laminate_photo_url),  
						array( '%s', '%s' )
					);
			}
		redirect('laminate', 'add');
    }    
	
	//Форма добавления товара
	echo
		"
			
			<form id='laminate_add' class='add_form' name='laminate_add' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=laminate&amp;tab=laminate_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('laminate_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='laminate_name' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Загрузить фото:</td>
					<td><input type='file' name='laminate_foto' accept='image/*' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='laminate_add_btn' type='submit' class='button' name='laminate_add_btn' value='Добавить' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

/*
	ПРОФИЛЯ!
*/

function profile_options_page() {
	if ( $_GET['page'] == 'profile' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=profile&tab=profile_list">';
		exit;
	}
	
	echo "<h2>Каталог профилей</h2><br />";
	
	$tab_pages = array( 'profile_list' => 'Список моделей профиля', 'profile_add' => 'Добавление профиля');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'profile'); else cupboard_admin_tabs($tab_pages, 'profile_list', 'profile');


	if ( $_GET['tab'] == 'profile_list' && $_GET['page'] == 'profile' ){
		profile_list_product();
	} else if ( $_GET['tab'] == 'profile_add' && $_GET['page'] == 'profile' ) {
		profile_add_product();
	}

}

function profile_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_profile;
	
	//Сохранение изменений товаров
	if ( isset($_POST['profile_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('profile_list_setup_form');
        }
		  
		//Файлы
		$profile_filelength = $_POST['profile_filelength'];
        
        $profile_color_url = array();
        $profile_list_id = $_POST['profile_list_id'];
        $profile_input_color_url = explode(", ", multiple_inputs('profile_input_color_url'));
        
        $profile_kind = explode(", ", stripslashes_deep(multiple_inputs('profile_kind')));
        
		if ( isset ($_FILES['profile_foto'])) {
			for ($i = 0; $i < $profile_filelength ; $i++) {
				if ($_FILES['profile_foto']['name'][$i] != ''){
				
				$file_name_new[$i] = translit($_FILES['profile_foto']['name'][$i]);
				$file_name_new_full[$i] = WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/'.$file_name_new[$i];
				
				if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/')) {
					mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/', 0755, true);
				}
				
				copy ( $_FILES['profile_foto']['tmp_name'][$i], $file_name_new_full[$i]);

				$fp[$i] = fopen($file_name_new_full[$i],"rb");
				$file[$i] = fread($fp[$i],filesize($file_name_new_full[$i]));
				fclose($fp[$i]); 
				
				$profile_color_url[$i] = '/wp-content/uploads/cupboardcalc/profile/'.$profile_list_id.'/'.translit($profile_kind[$i]).'/'.$file_name_new[$i];

			}
			
			if ($profile_color_url[$i] != null && ($profile_input_color_url[$i] == '0' || $profile_input_color_url[$i] != $profile_color_url[$i])) {
				$profile_color_url[$i] = $profile_color_url[$i];
			} else $profile_color_url[$i] = $profile_input_color_url[$i];
		}
			
		}
		
		//Файлы 2
		$profile_type_filelength = $_POST['profile_type_filelength'];
        
        $profile_type_url = array();
        $profile_input_type_url = explode(", ", multiple_inputs('profile_input_type_url'));
        
        $profile_type = explode(", ", stripslashes_deep(multiple_inputs('profile_type')));
		
		if ( isset ($_FILES['profile_type_foto'])) {
			for ($i = 0; $i < $profile_type_filelength ; $i++) {
				if ($_FILES['profile_type_foto']['name'][$i] != ''){
				
				$file_name_new[$i] = translit($_FILES['profile_type_foto']['name'][$i]);
				$file_name_new_full[$i] = WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/'.$file_name_new[$i];
				
				if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/')) {
					mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.translit($profile_kind[$i]).'/', 0755, true);
				}
				
				copy ( $_FILES['profile_type_foto']['tmp_name'][$i], $file_name_new_full[$i]);

				$fp[$i] = fopen($file_name_new_full[$i],"rb");
				$file[$i] = fread($fp[$i],filesize($file_name_new_full[$i]));
				fclose($fp[$i]); 
				
				$profile_type_url[$i] = '/wp-content/uploads/cupboardcalc/profile/'.$profile_list_id.'/'.translit($profile_kind[$i]).'/'.$file_name_new[$i];

			}
			
			if ($profile_type_url[$i] != null && ($profile_input_type_url[$i] == '0' || $profile_input_type_url[$i] != $profile_type_url[$i])) {
				$profile_type_url[$i] = $profile_type_url[$i];
			} else $profile_type_url[$i] = $profile_input_type_url[$i];
		}
			
		}
				
		$profile_color_url = implode(", ", $profile_color_url);
		$profile_type_url = implode(", ", $profile_type_url);
		  
		$profile_name = stripslashes_deep($_POST['profile_name']);
		$profile_panels = multiple_inputs('profile_panels');
		$profile_color = stripslashes_deep(multiple_inputs('profile_color'));
		$profile_type = implode(", ", $profile_type);
		$profile_kind = implode(", ", $profile_kind);
		$profile_table_colorType = stripslashes_deep(multiple_inputs('profile_table_colorType'));
		$profile_table_topFrame = multiple_inputs('profile_table_topFrame');
		$profile_table_bottomFrame = multiple_inputs('profile_table_bottomFrame');
		$profile_table_divider = multiple_inputs('profile_table_divider');
		$profile_table_topTrack = multiple_inputs('profile_table_topTrack');
		$profile_table_bottomTrack = multiple_inputs('profile_table_bottomTrack');
		$profile_table_handle = multiple_inputs('profile_table_handle');
			
		$wpdb->update
				(
					$table_products,  
					array( 'profile_name' => $profile_name, 'profile_panels' => $profile_panels, 'profile_color' => $profile_color, 'profile_color_url' => $profile_color_url,
					'profile_type' => $profile_type, 'profile_kind' => $profile_kind, 'profile_type_url' => $profile_type_url, 'profile_table_colorType' => $profile_table_colorType, 'profile_table_topFrame' => $profile_table_topFrame,
					'profile_table_bottomFrame' => $profile_table_bottomFrame, 'profile_table_divider' => $profile_table_divider, 'profile_table_topTrack' => $profile_table_topTrack, 
					'profile_table_bottomTrack' => $profile_table_bottomTrack, 'profile_table_handle' => $profile_table_handle),
					array( 'id' => $profile_list_id ),  
					array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s'),
					array( '%d' )
					);
					
		redirect('profile', 'list');	
		
    }
	
	//Удаление товара
	if ( isset($_POST['profile_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('profile_list_setup_form');
        }
		  
		$profile_list_id = $_POST['profile_list_id'];

		$profile_color_url_now = $wpdb->get_var( "SELECT profile_color_url FROM $table_products WHERE id=".$profile_list_id);
        $profile_type_url_now = $wpdb->get_var( "SELECT profile_type_url FROM $table_products WHERE id=".$profile_list_id);
        $profile_color_url = explode(", ", $profile_color_url_now);
        $profile_type_url = explode(", ", $profile_type_url_now);

        for ($i = 0, $l = count($profile_color_url); $i < $l; $i++) {
            unlink(url2path(home_url().$profile_color_url[$i]));
        }

		for ($i = 0, $l = count($profile_type_url); $i < $l; $i++) {
            unlink(url2path(home_url().$profile_type_url[$i]));
        }
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $profile_list_id");
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации	
		$profile_panels = explode(", ", $item->profile_panels);
		$profile_color = stripslashes_deep(explode(", ", $item->profile_color));
		$profile_color_url = explode(", ", $item->profile_color_url);
		$profile_type = stripslashes_deep(explode(", ", $item->profile_type));
		$profile_kind = stripslashes_deep(explode(", ", $item->profile_kind));
		$profile_type_url = explode(", ", $item->profile_type_url);
		$profile_table_colorType = stripslashes_deep(explode(", ", $item->profile_table_colorType));
		$profile_table_topFrame = explode(", ", $item->profile_table_topFrame);
		$profile_table_bottomFrame = explode(", ", $item->profile_table_bottomFrame);
		$profile_table_divider = explode(", ", $item->profile_table_divider);
		$profile_table_topTrack = explode(", ", $item->profile_table_topTrack);
		$profile_table_bottomTrack = explode(", ", $item->profile_table_bottomTrack);
		$profile_table_handle = explode(", ", $item->profile_table_handle);
		
		$options = '';
		
		echo  
		"	
		
			<form id='profile_list' class='list_form' name='profile_list_setup' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=profile&amp;tab=profile_list&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('profile_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='profile_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Вид:</td>
					<td><input type='text' name='profile_name' value='".$item->profile_name."' style='width:300px;'/></td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Цена за штуку для предметов:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>
						<label> Стопор <input type='text' name='profile_panels[]' style='width:50px;' value='".$profile_panels[0]."' /></label>
						<label> Щетка <input type='text' name='profile_panels[]' style='width:50px;' value='".$profile_panels[1]."' /></label>
						<label> Ролик <input type='text' name='profile_panels[]' style='width:50px;' value='".$profile_panels[2]."' /></label>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				
				<tr class='profile_add_type_raw'>
					<td style='text-align:right;'>Варианты типов ручки:</td>
					<td style='text-align:left;'><button class='profile_add_type'>Добавить вариант</button><input type='hidden' class='profile_type_filelength' value='".count($profile_type)."' name='profile_type_filelength' /></td>
					<td>&nbsp;</td>";
					
					for($i = 0; $i < count($profile_type); $i++) {
						
						echo "<tr class='profile_type_raw'><td></td><td>Название: <input type='text' class='profile_type' for='profile_kind_".($i+1)."' name='profile_type[]' value='".$profile_type[$i]."' style='width:150px;'/> Фото: 
							<input type='file' name='profile_type_foto[]' accept='image/*' style='width:300px;'/><a class='profile_delete_type'>X</a><br /><br />
							<img src='".home_url().$profile_type_url[$i]."' alt='Фото типа ручки' /><input type='hidden' class='profile_input_type_url' name='profile_input_type_url[]' style='width:50px;' value='".$profile_type_url[$i]."' /></td></tr>";						
						
					}
					
				echo "
				
				<tr>
					<td><br /></td>
				</tr>
				
				<tr class='profile_add_color_raw'>
					<td style='text-align:right;'>Варианты цвета по виду профиля:</td>
					<td style='text-align:left;'><button class='profile_add_color'>Добавить вариант</button><input type='hidden' class='profile_filelength' value='".count($profile_color)."' name='profile_filelength' /></td>			
						
					<td>&nbsp;</td>";
				
					
				
				for($i = 0; $i < count($profile_color); $i++) {
					
					$options = '';
					
					for($a = 0; $a < count($profile_type); $a++) {
							$profile_type[$a]==$profile_kind[$i] ? $selected = "selected" : $selected = '';
							$options = $options."<option class='profile_kind_".($a+1)."' value='".$profile_type[$a]."' ".$selected.">".$profile_type[$a]."</option>";
						}
				
				
					echo "<tr class='profile_color_raw'><td></td><td>Название цвета: <input type='text' class='profile_color' name='profile_color[]' style='width:150px;' value='".$profile_color[$i]."'/> Вид профиля: <select type='text' class='profile_kind' name='profile_kind[]' style='width:150px;'>".$options." </select> Фото: 
					<input type='file' name='profile_foto[]' accept='image/*' style='width:300px;'/><a class='profile_delete_color'>X</a><br /><br />
					<img src='".home_url().$profile_color_url[$i]."' alt='Фото цвета профиля' /><input type='hidden' class='profile_input_color_url' name='profile_input_color_url[]' style='width:50px;' value='".$profile_color_url[$i]."' /></td></tr>";
				}
				
				echo "
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Таблица Цен<br/>(заполняется на<br /> основании ширины):</td>
					<td>
					<button class='profile_add_colorType_table_reload'>Обновить Таблицу цен</button>
					<br /><br />
					<table class='profile_add_colorType_table'>
						<tr class='profile_add_raw_colorType'>
							<td class='first_cell'>Цвет/тип</td>";
							
							for($i = 0; $i < count($profile_table_colorType); $i++) {
								echo "<td><p>".$profile_table_colorType[$i]."</p><input type='hidden' class='profile_add_input_colorType' name='profile_table_colorType[]' style='width:50px;' value='".$profile_table_colorType[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_topFrame'>
							<td class='first_cell'>Цена погонного метра рамки верхней</td>";
							
							for($i = 0; $i < count($profile_table_topFrame); $i++) {
								echo "<td><input type='text' class='profile_add_input_topFrame' name='profile_table_topFrame[]' style='width:50px;' value='".$profile_table_topFrame[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_bottomFrame'>
							<td class='first_cell'>Цена погонного метра рамки нижней</td>";
							
							for($i = 0; $i < count($profile_table_bottomFrame); $i++) {
								echo "<td><input type='text' class='profile_add_input_bottomFrame' name='profile_table_bottomFrame[]' style='width:50px;' value='".$profile_table_bottomFrame[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_divider'>
							<td class='first_cell'>Цена погонного метра для делителя</td>";
							
							for($i = 0; $i < count($profile_table_divider); $i++) {
								echo "<td><input type='text' class='profile_add_input_divider' name='profile_table_divider[]' style='width:50px;' value='".$profile_table_divider[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_topTrack'>
							<td class='first_cell'>Цена погонного метра для верхнего трека</td>";
							
							for($i = 0; $i < count($profile_table_topTrack); $i++) {
								echo "<td><input type='text' class='profile_add_input_topTrack' name='profile_table_topTrack[]' style='width:50px;' value='".$profile_table_topTrack[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_bottomTrack'>
							<td class='first_cell'>Цена погонного метра для нижнего трека</td>";
							
							for($i = 0; $i < count($profile_table_bottomTrack); $i++) {
								echo "<td><input type='text' class='profile_add_input_bottomTrack' name='profile_table_bottomTrack[]' style='width:50px;' value='".$profile_table_bottomTrack[$i]."' /></td>";
							}
							
						echo "</tr>
						<tr class='profile_add_raw_handle'>
							<td class='first_cell'>Цена за 1 штуку ручки каждого типа</td>";
							
							for($i = 0; $i < count($profile_table_handle); $i++) {
								echo "<td><input type='text' class='profile_add_input_handle' name='profile_table_handle[]' style='width:50px;' value='".$profile_table_handle[$i]."' /></td>";
							}
							
						echo "</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='profile_add_btn'type='submit' class='button' name='profile_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='profile_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}

function profile_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_profile;
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	
	$profile_list_id=end($products)->id+1;
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['profile_add_btn']) ) 
    {
    
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('profile_add_form');
        }
        
        //Файлы
        
        $profile_filelength = $_POST['profile_filelength'];
        $profile_type_filelength = $_POST['profile_type_filelength'];
        
        $profile_color_url = array();
        $profile_type_url = array();
        
        $profile_kind = explode(", ", stripslashes_deep(multiple_inputs('profile_kind')));
        $profile_type = explode(", ", stripslashes_deep(multiple_inputs('profile_type')));
        
		if ( isset ($_FILES['profile_foto'])) {
			for ($i = 0; $i < $profile_filelength ; $i++) {

				$file_name_new[$i] = translit($_FILES['profile_foto']['name'][$i]);
				$file_name_new_full[$i] = WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_kind[$i].'/'.$file_name_new[$i];
				
				if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_kind[$i].'/')) {
					mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_kind[$i].'/', 0755, true);
				}
				
				copy ( $_FILES['profile_foto']['tmp_name'][$i], $file_name_new_full[$i]);

				$fp[$i] = fopen($file_name_new_full[$i],"rb");
				$file[$i] = fread($fp[$i],filesize($file_name_new_full[$i]));
				fclose($fp[$i]); 

				$profile_color_url[$i] = '/wp-content/uploads/cupboardcalc/profile/'.$profile_list_id.'/'.$profile_kind[$i].'/'.$file_name_new[$i];
			}
		}
		
		if ( isset ($_FILES['profile_type_foto'])) {
			for ($i = 0; $i < $profile_type_filelength ; $i++) {

				$file_name_new[$i] = translit($_FILES['profile_type_foto']['name'][$i]);
				$file_name_new_full[$i] = WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_type[$i].'/'.$file_name_new[$i];
				
				if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_type[$i].'/')) {
					mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/profile/".$profile_list_id.'/'.$profile_type[$i].'/', 0755, true);
				}
				
				copy ( $_FILES['profile_type_foto']['tmp_name'][$i], $file_name_new_full[$i]);

				$fp[$i] = fopen($file_name_new_full[$i],"rb");
				$file[$i] = fread($fp[$i],filesize($file_name_new_full[$i]));
				fclose($fp[$i]); 

				$profile_type_url[$i] = '/wp-content/uploads/cupboardcalc/profile/'.$profile_list_id.'/'.$profile_type[$i].'/'.$file_name_new[$i];
			}
		}
		
		
		  
		$profile_name = stripslashes_deep($_POST['profile_name']);
		$profile_panels = multiple_inputs('profile_panels');
		$profile_color = stripslashes_deep(multiple_inputs('profile_color'));
		$profile_color_url = implode(", ", $profile_color_url);
		$profile_type = implode(", ", $profile_type);
		$profile_kind = implode(", ", $profile_kind);
		$profile_type_url = implode(", ", $profile_type_url);
		$profile_table_colorType = stripslashes_deep(multiple_inputs('profile_table_colorType'));
		$profile_table_topFrame = multiple_inputs('profile_table_topFrame');
		$profile_table_bottomFrame = multiple_inputs('profile_table_bottomFrame');
		$profile_table_divider = multiple_inputs('profile_table_divider');
		$profile_table_topTrack = multiple_inputs('profile_table_topTrack');
		$profile_table_bottomTrack = multiple_inputs('profile_table_bottomTrack');
		$profile_table_handle = multiple_inputs('profile_table_handle');
			
		$wpdb->insert
				(
					$table_products,  
					array( 'profile_name' => $profile_name, 'profile_panels' => $profile_panels, 'profile_color' => $profile_color, 'profile_color_url' => $profile_color_url, 'profile_kind' => $profile_kind,
					'profile_type' => $profile_type, 'profile_type_url' => $profile_type_url, 'profile_table_colorType' => $profile_table_colorType, 'profile_table_topFrame' => $profile_table_topFrame,
					'profile_table_bottomFrame' => $profile_table_bottomFrame, 'profile_table_divider' => $profile_table_divider, 'profile_table_topTrack' => $profile_table_topTrack, 
					'profile_table_bottomTrack' => $profile_table_bottomTrack, 'profile_table_handle' => $profile_table_handle),  
					array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
					);
					
		redirect('profile', 'add');
    
    }
    
    	
    
    
	
	//Форма добавления товара
	echo
		"
			
			<form id='profile_add' class='add_form' name='profile_add' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=profile&amp;tab=profile_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('profile_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Серия:</td>
					<td><input type='text' name='profile_name' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Цена за штуку для предметов:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td><label> Стопор <input type='text' name='profile_panels[]' style='width:50px;' /></label>
					<label> Щетка <input type='text' name='profile_panels[]' style='width:50px;' /></label>
					<label> Ролик <input type='text' name='profile_panels[]' style='width:50px;' /></label></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr class='profile_add_type_raw'>
					<td style='text-align:right;'>Варианты типов ручки:</td>
					<td style='text-align:left;'><button class='profile_add_type'>Добавить вариант</button><input type='hidden' class='profile_type_filelength' value='1' name='profile_type_filelength' /></td>
				<tr class='profile_type_raw'>
					<td>&nbsp;</td>
					<td>Название: <input type='text' class='profile_type' name='profile_type[]' for='profile_kind_1' style='width:150px;'/> Фото: <input type='file' name='profile_type_foto[]' accept='image/*' style='width:300px;'/></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr class='profile_add_color_raw'>
					<td style='text-align:right;'>Варианты цвета по виду профиля:</td>
					<td style='text-align:left;'><button class='profile_add_color'>Добавить вариант</button><input type='hidden' class='profile_filelength' value='1' name='profile_filelength' /></td>
				<tr class='profile_color_raw'>
					<td>&nbsp;</td>
					<td>Название цвета: <input type='text' class='profile_color' name='profile_color[]' style='width:150px;'/> Вид профиля: <select type='text' class='profile_kind' name='profile_kind[]' style='width:150px;'><option class='profile_kind_1'></option> </select> Фото: <input type='file' name='profile_foto[]' accept='image/*' style='width:300px;'/></td>
				</tr>
				
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>
						Таблица Цен:
					</td>
					<td>
					<button class='profile_add_colorType_table_reload'>Обновить Таблицу цен</button>
					<br /><br />
					<table class='profile_add_colorType_table'>
						<tr class='profile_add_raw_colorType'>
							<td class='first_cell'>Цвет/тип</td>	
						</tr>
						<tr class='profile_add_raw_topFrame'>
							<td class='first_cell'>Цена погонного метра рамки верхней</td>
						</tr>
						<tr class='profile_add_raw_bottomFrame'>
							<td class='first_cell'>Цена погонного метра рамки нижней</td>
						</tr>
						<tr class='profile_add_raw_divider'>
							<td class='first_cell'>Цена погонного метра для делителя</td>
						</tr>
						<tr class='profile_add_raw_topTrack'>
							<td class='first_cell'>Цена погонного метра для верхнего трека</td>
						</tr>
						<tr class='profile_add_raw_bottomTrack'>
							<td class='first_cell'>Цена погонного метра для нижнего трека</td>
						</tr>
						<tr class='profile_add_raw_handle'>
							<td class='first_cell'>Цена за 1 штуку ручки каждого типа</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='profile_add_btn' type='submit' class='button' name='profile_add_btn' value='Добавить профиль' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

// РАМКИ

function frame_options_page() {
	if ( $_GET['page'] == 'frame' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=frame&tab=frame_list">';
		exit;
	}
	
	echo "<h2>Каталог рамок</h2><br />";
	
	$tab_pages = array( 'frame_list' => 'Список моделей рамок', 'frame_add' => 'Добавить рамку');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'frame'); else cupboard_admin_tabs($tab_pages, 'frame_list', 'frame');


	if ( $_GET['tab'] == 'frame_list' && $_GET['page'] == 'frame' ){
		frame_list_product();
	} else if ( $_GET['tab'] == 'frame_add' && $_GET['page'] == 'frame' ) {
		frame_add_product();
	}

}

function frame_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_frame;
	
	//Сохранение изменений товаров
	if ( isset($_POST['frame_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('frame_list_setup_form');
        }
		  
		$frame_list_id = $_POST['frame_list_id'];			
		
		$frame_name = stripslashes_deep($_POST['frame_name']);
		
		$frame_id = multiple_inputs('frame_id', 1);
		$frame_color = multiple_inputs('frame_color', 1);
		$frame_height = multiple_inputs('frame_height', 1);
		$frame_width = multiple_inputs('frame_width', 1);
		$frame_top = multiple_inputs('frame_top', 1);
		$frame_left = multiple_inputs('frame_left', 1);
		$frame_fix_w = multiple_inputs('frame_fix_w', 1);
		$frame_fix_h = multiple_inputs('frame_fix_h', 1);
		
		
		for($i = 0; $i < count($frame_id); $i++) {
		
			$frame_array[$i] = array('frame_id' => $frame_id[$i],
									 'frame_color' => $frame_color[$i],
									 'frame_height' => $frame_height[$i],
									 'frame_width' => $frame_width[$i],
									 'frame_top' => $frame_top[$i],
									 'frame_left' => $frame_left[$i],
									 'frame_fix_w' => $frame_fix_w[$i],
									 'frame_fix_h' => $frame_fix_h[$i]);
		}
		
		$frame_array = json_encode($frame_array);

		$wpdb->update
				(
					$table_products,  
					array( 'frame_name' => $frame_name, 'frame_array' => $frame_array ),
					array( 'id' => $frame_list_id ),   
					array( '%s', '%s' ),
					array( '%d' )
					);
					
		redirect('frame', 'list');

    }
    	
	
	//Удаление товара
	if ( isset($_POST['frame_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('frame_list_setup_form');
        }
		  
		$frame_list_id = $_POST['frame_list_id'];
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $frame_list_id"); 
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	
	$item_number = 0;
	
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		
		$frame_array = json_decode($item->frame_array, true);
		
		echo
		"
			<form id='frame_list' class='list_form frame_form' name='frame_list_setup' method='post' action='".$_SERVER['PHP_SELF']."?page=frame&amp;tab=frame_list&amp;updated=true'>
			<h3 class='form_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('frame_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='frame_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='frame_name' style='width:250px;' value='".$item->frame_name."'/></td>
					<td>Введите название рамки, скорректируйте размеры<br /> 
					и пропорции, введя их в соответсвующие поля,<br />
					и кликнув на любом свободном месте</td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>базовый размер - 2300мм*800мм</td>
					<td style='text-align:right;'>пропорции 1: <input type='text' class='frame_proportion' name='frame_proportion' style='width:50px;' value='10'/> px:мм</td>
					<td><input type='hidden' class='frame_old_proportion' name='frame_old_proportion' style='width:50px;' value='10'/></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>
						<div class='frame_add_area left'>";
						
						for($i = 0; $i < count($frame_array); $i++) {
						
							echo "<div id='frame_".$frame_array[$i]['frame_id']."' class='frame_div'
							
							style = '
								top:".$frame_array[$i]['frame_top']."px; 
								left:".$frame_array[$i]['frame_left']."px;
								background-color:".$frame_array[$i]['frame_color'].";
								height:".($frame_array[$i]['frame_height']/10)."px;
								width:".($frame_array[$i]['frame_width']/10)."px;
								'
							
							>".$frame_array[$i]['frame_id']."</div>";
							
						}
							
				echo    "</div>
						
					</td>
					<td class='frame_add_list'>
						<div class='frame_add_div'><button class='frame_add_doorFrame'>Добавить рамку</button></div>";
						
						
						
						for($i = 0; $i < count($frame_array); $i++) {
							
							echo "
							
							<div id='frame_param_".$frame_array[$i]['frame_id']."' class='right frame_param'>
							Рамка: ".$frame_array[$i]['frame_id']."
							 Ширина: <input type='text' class='frame_width' name='frame_width[]' style='width:50px;' value='".$frame_array[$i]['frame_width']."'/> мм; 
							Высота: <input type='text' class='frame_height' name='frame_height[]' style='width:50px;' value='".$frame_array[$i]['frame_height']."'/> мм;
							
							Верт. фикс: <input type='checkbox' name='frame_fix_h[]' data-for='frame_fix_h_hidden' value='1' ";
							if ($frame_array[$i]['frame_fix_h'] == '1' ) echo "checked";
							echo " />
							<input class='frame_fix_h_hidden' type='hidden' value='0' name='frame_fix_h[]' ";
							if ($frame_array[$i]['frame_fix_h'] == '1' ) echo "disabled";
							echo " />
							
							Гор. фикс: <input type='checkbox' name='frame_fix_w[]' data-for='frame_fix_w_hidden' value='1' ";
							if ($frame_array[$i]['frame_fix_w'] == '1' ) echo "checked";
							echo " />
							<input class='frame_fix_w_hidden' type='hidden' value='0' name='frame_fix_w[]' ";
							if ($frame_array[$i]['frame_fix_w'] == '1' ) echo "disabled";
							echo " />
							<input type='hidden' class='frame_id' name='frame_id[]' value='".$frame_array[$i]['frame_id']."'/>
							<input type='hidden' name='frame_color[]' value='".$frame_array[$i]['frame_color']."'/>
							<input type='hidden' class='frame_top' name='frame_top[]' value='".$frame_array[$i]['frame_top']."'/>
							<input type='hidden' class='frame_left' name='frame_left[]' value='".$frame_array[$i]['frame_top']."'/>
							<a class='frame_delete_doorFrame'> X </a></div>";
							
						}	
						
				echo "</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='frame_add_btn'type='submit' class='button' name='frame_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='frame_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
		
		$item_number += 1;
	}
}

function frame_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_frame;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['frame_add_btn']) ) 
    {
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('frame_add_form');
        }
		
		$frame_name = stripslashes_deep($_POST['frame_name']);
		
		$frame_id = multiple_inputs('frame_id', 1);
		$frame_color = multiple_inputs('frame_color', 1);
		$frame_height = multiple_inputs('frame_height', 1);
		$frame_width = multiple_inputs('frame_width', 1);
		$frame_top = multiple_inputs('frame_top', 1);
		$frame_left = multiple_inputs('frame_left', 1);
		$frame_fix_w = multiple_inputs('frame_fix_w', 1);
		$frame_fix_h = multiple_inputs('frame_fix_h', 1);
		
		for($i = 0; $i < count($frame_id); $i++) {
		
			$frame_array[$i] = array('frame_id' => $frame_id[$i],
									 'frame_color' => $frame_color[$i],
									 'frame_height' => $frame_height[$i],
									 'frame_width' => $frame_width[$i],
									 'frame_top' => $frame_top[$i],
									 'frame_left' => $frame_left[$i],
									 'frame_fix_w' => $frame_fix_w[$i],
									 'frame_fix_h' => $frame_fix_h[$i]);
			
		}
		
		$frame_array = json_encode($frame_array);

		$wpdb->insert
				(
					$table_products,  
					array( 'frame_name' => $frame_name, 'frame_array' => $frame_array ),  
					array( '%s', '%s' )
					);
		redirect('frame', 'add');
    }
    
    
	
	//Форма добавления товара
	echo
		"
			
			<form id='frame_add' class='frame_form' name='frame_form' method='post' action='".$_SERVER['PHP_SELF']."?page=frame&amp;tab=frame_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('frame_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='frame_name' style='width:250px;'/></td>
					<td>Введите название рамки, скорректируйте размеры<br /> 
					и пропорции, введя их в соответсвующие поля,<br />
					и кликнув на любом свободном месте</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>базовый размер - 2300мм*800мм</td>
					<td style='text-align:right;'>пропорции 1: <input type='text' class='frame_proportion' name='frame_proportion' style='width:50px;' value='10'/> px:мм</td>
					<td>
						<input type='hidden' class='frame_old_proportion' name='frame_old_proportion' style='width:30px;' value='10'/>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>
						<div class='frame_add_area left'></div>
						
					</td>
					<td class='frame_add_list'>
						<div class='frame_add_div'><button class='frame_add_doorFrame'>Добавить рамку</button></div>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='frame_add_btn' type='submit' class='button' name='frame_add_btn' value='Добавить' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

/*
	ЗАПОЛНЕНИЯ!
*/

    

 /*
 
/**
 * Set Upload Directory
 *
 * Sets the upload dir to edd. This function is called from
 * edd_change_downloads_upload_dir()
 *
 * @since 1.0
 * @return array Upload directory information
*/
function edd_set_upload_dir( $upload ) {
    
    $fs =  "cupboardcalc/filler";
    
    $x = $_SERVER['HTTP_REFERER'];
    $pattern = "/page=image/";
    preg_match($pattern, $x, $match);
    
    if (count($match)>0) {
    
    $upload['subdir'] =  '/'.$fs.$upload['subdir'];
    $upload['path'] = $upload['basedir'] . $upload['subdir'];
    $upload['url']  = $upload['baseurl'] . $upload['subdir'];
    
    }
    
    return $upload;
}

add_filter( 'upload_dir', 'edd_set_upload_dir' );

function image_options_page() {
	if ( $_GET['page'] == 'image' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=image&tab=image_list">';
		exit;
	}
	
	echo "<h2>Каталог вариантов заполнений</h2><br />";
	
	$tab_pages = array( 'image_list' => 'Список вариантов заполнений', 'image_add' => 'Добавить вариант заполнения');

    //generic HTML and code goes here
        

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'image'); else cupboard_admin_tabs($tab_pages, 'image_list', 'image');


	if ( $_GET['tab'] == 'image_list' && $_GET['page'] == 'image' ){
		image_list_product();
	} else if ( $_GET['tab'] == 'image_add' && $_GET['page'] == 'image' ) {
		image_add_product();
	}

}

function image_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_image;
	
	//Сохранение изменений товаров
	if ( isset($_POST['image_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('image_list_setup_form');
        }
		  
		$image_list_id = $_POST['image_list_id'];			
		
		$image_name = stripslashes_deep(multiple_inputs('imageGalleryName', 1));
		$image_url = multiple_inputs('imageGalleryURL', 1);
		$image_caption = multiple_inputs('imageGalleryCaption', 1);
		$image_ids = multiple_inputs('imageGalleryIDs', 1);
		$image_sub = multiple_inputs('imageGallerySub', 1);

		$image_cost = multiple_inputs('imageCost', 1);
		$image_fillMethod = $_POST['fillMethod'];
		
		for($i = 0; $i < count($image_name); $i++) {
		
			$image_array[$i] = array('image_name' => $image_name[$i],
									 'image_url' => $image_url[$i],
									 'image_caption' => $image_caption[$i],
									 'image_ids' => $image_ids[$i],
									 'image_sub' => $image_sub[$i]);
									 
		}
		
		$image_array = json_encode($image_array);
		$image_cost = json_encode($image_cost);

		$wpdb->update
				(
					$table_products,  
					array( 'image_array' => $image_array, 'image_cost' => $image_cost, 'image_fillMethod' => $image_fillMethod ),
					array( 'id' => $image_list_id ),   
					array( '%s', '%s' ),
					array( '%d' )
					);
					
		redirect('image', 'list');

    }
	
	//Удаление товара
	if ( isset($_POST['image_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('image_list_setup_form');
        }
		  
		$image_list_id = $_POST['image_list_id'];
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $image_list_id"); 
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		
		$image_array = json_decode($item->image_array, true);
		$image_cost = json_decode($item->image_cost, true);
		
		echo
		"
			<form id='image_list' class='list_form image_form' name='image_list_setup' method='post' action='".$_SERVER['PHP_SELF']."?page=image&amp;tab=image_list&amp;updated=true'>
			<h3 class='form_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('image_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='image_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'></td>
					<td><div class='category_imageGallery'>
						Название основной категории: <input type='text' name='imageGalleryName[]' placeholder='Введите название' style='width:250px;' value='".$image_array[0]['image_name']."'/>";
						
						if ($image_array[0]['image_url'] != '') {
							
							echo "<a class='upload-and-attach-link' href='#'>
							<img src='".plugins_url( 'css/edit.png' , __FILE__ )."' id='wp_editgallery' width='24' height='24' title='Редактировать галерею'> </a>";
							};
							
							echo "
						<button class='add_subcategory_imageGallery'> Добавить подкатегорию</button>
						<input type='button' class='show_list collapsed' value='Показать'>
						<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' value='".$image_array[0]['image_url']."'/>
						<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' value='".$image_array[0]['image_caption']."'/>
						<input type='hidden' name='imageGalleryIDs[]' class='imageGalleryIDs' value='".$image_array[0]['image_ids']."'/>
						<input type='hidden' name='imageGallerySub[]' class='imageGallerySub' value='".$image_array[0]['image_sub']."'/>";
						
						for($i = 1; $i < count($image_array); $i++) {
							
							if ($image_array[$i]['image_sub'] == 'subcategory') {
								$category_imageGallery = 'subcategory_imageGallery';
								$button = "<button class='add_subcategory_imageGallery'>Добавить субподкатегорию</button>";
							} else {
								$category_imageGallery = 'subsubcategory_imageGallery';
								$button = '';
							}
						
							
							echo "
							
							<div class='category_imageGallery ".$image_array[$i]['image_sub']."'>
							<input class='".$category_imageGallery."' type='text' name='imageGalleryName[]' placeholder='Введите название' value='".$image_array[$i]['image_name']."'/>
							<input type='text' name='imageCost[]' placeholder='Спец цена' value='".$image_cost[$i-1]."'/>";
							
							if ($image_array[$i]['image_url'] != '') {
							
							echo "<a class='upload-and-attach-link' href='#'>
							<img src='".plugins_url( 'css/edit.png' , __FILE__ )."' id='wp_editgallery' width='24' height='24' title='Редактировать галерею'> </a>";
							}
							
							echo $button."
							<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' value='".$image_array[$i]['image_url']."'/>
							<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' value='".$image_array[$i]['image_caption']."'/>
							<input type='hidden' name='imageGalleryIDs[]' class='imageGalleryIDs' value='".$image_array[$i]['image_ids']."'/>
							<input type='hidden' name='imageGallerySub[]' class='imageGallerySub' value='".$image_array[$i]['image_sub']."'/>
							<a class='imageGallery_delete_category'> X</a>";
							
							if ($image_array[$i]['image_sub'] == 'subcategory' && $image_array[$i+1]['image_sub'] == 'subsubcategory') {
								
							} else echo "</div>";
							
							if ($image_array[$i]['image_sub'] == 'subsubcategory' && $image_array[$i+1]['image_sub'] == 'subcategory') {
								echo "</div>";
							}
							
						}
						
						
					echo "</div>
					</div>
					</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>Выберите способ заполнения:
					<label> Расстягивание <input type='radio' name='fillMethod' value='0'";
					if ($item->image_fillMethod == 0) echo "checked";
					echo "/></label>
					<label> Заливка <input type='radio' name='fillMethod' value='1'";
					if ($item->image_fillMethod == 1) echo "checked";
					echo "/></label></td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>Стоимость кв/м заполнения(руб): <input type='text' name='imageCost[]' placeholder='Введите стоимость' style='width:150px;' value='".end($image_cost)."'/></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='image_add_btn'type='submit' class='button' name='image_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='image_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}


function image_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_image;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['image_add_btn']) ) 
    {
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('image_add_form');
        }
		
		$image_name = stripslashes_deep(multiple_inputs('imageGalleryName', 1));
		$image_url = multiple_inputs('imageGalleryURL', 1);
		$image_caption = multiple_inputs('imageGalleryCaption', 1);
		$image_ids = multiple_inputs('imageGalleryIDs', 1);
		$image_sub = multiple_inputs('imageGallerySub', 1);

		//$image_cost = $_POST['imageCost'];
		$image_cost = multiple_inputs('imageCost', 1);
		
		$image_fillMethod = $_POST['fillMethod'];
		
		for($i = 0; $i < count($image_name); $i++) {
		
			$image_array[$i] = array('image_name' => $image_name[$i],
									 'image_url' => $image_url[$i],
									 'image_caption' => $image_caption[$i],
									 'image_ids' => $image_ids[$i],
									 'image_sub' => $image_sub[$i]);
			
		}
		
		$image_array = json_encode($image_array);
		$image_cost = json_encode($image_cost);
		
		$wpdb->insert
				(
					$table_products,  
					array( 'image_array' => $image_array, 'image_cost' => $image_cost, 'image_fillMethod' => $image_fillMethod ),  
					array( '%s', '%s', '%s' )
					);
					
		redirect('image', 'add');    
    }
	
	//Форма добавления товара
	?>

	<?php
	echo
		"
			
			<form id='image_add' class='image_form add_form' name='image_form' method='post' action='".$_SERVER['PHP_SELF']."?page=image&amp;tab=image_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('image_add_form'); 
		}
	
	echo
	"
			<table>
				
				<tr>
					<td style='text-align:right;'></td>
					<td>
					<div class='category_imageGallery'>
						Название основной категории:<input type='text' name='imageGalleryName[]' placeholder='Введите название' style='width:250px;'/>
						<a class='main_category_imageGallery upload-and-attach-link' id='main_category' href='#'>
						<img src='".plugins_url( 'css/edit.png' , __FILE__ )."' id='wp_editgallery' width='24' height='24' title='Редактировать галерею'>
						</a>
						<button class='add_subcategory_imageGallery'> Добавить подкатегорию</button>
						<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' />
						<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' />
						<input type='hidden' name='imageGalleryIDs[]' class='imageGalleryIDs' />
						<input type='hidden' name='imageGallerySub[]' class='imageGallerySub' value='main_category'/>
					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>Стоимость кв/м заполнения(руб): <input type='text' name='imageCost[]' placeholder='Введите стоимость' style='width:150px;'/></td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td>Выберите способ заполнения: <label> Расстягивание <input type='radio' name='fillMethod' value='0' /></label> <label> Заливка <input type='radio' name='fillMethod' value='1'/></label></td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td><p>
										</p>
					</div>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				
				
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='image_add_btn' type='submit' class='button' name='image_add_btn' value='Добавить вариант заполнения' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

/*
	УГЛОВОЙ СЕГМЕНТ!
*/

function corner_options_page() {
	if ( $_GET['page'] == 'corner' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=corner&tab=corner_list">';
		exit;
	}
	
	echo "<h2>Каталог корпусов</h2><br />";
	
	$tab_pages = array( 'corner_list' => 'Список угловых сегментов', 'corner_add' => 'Добавить угловой сегмент');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'corner'); else cupboard_admin_tabs($tab_pages, 'corner_list', 'corner');


	if ( $_GET['tab'] == 'corner_list' && $_GET['page'] == 'corner' ){
		corner_list_product();
	} else if ( $_GET['tab'] == 'corner_add' && $_GET['page'] == 'corner' ) {
		corner_add_product();
	}

}

function corner_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corner;
	
	//Сохранение изменений товаров
	if ( isset($_POST['corner_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corner_list_setup_form');
        }
		  
		//Файлы
		if ( isset ($_FILES['corner_foto']) && $_FILES['corner_foto']['name'] != '') {
			$file_name_new = translit($_FILES['corner_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/corner/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/corner/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/corner/", 0755, true);
			}
			
			copy ( $_FILES['corner_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 

		}
		
		$corner_list_id = $_POST['corner_list_id'];
		
		//проверка фото, в случае несовпадения переписывает
		$corner_photo_url_now = $wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$corner_list_id );
		$corner_photo_url = '/wp-content/uploads/cupboardcalc/corner/'.$file_name_new;
		$file_name_new != null ? $corner_photo_url = $corner_photo_url : $corner_photo_url = $corner_photo_url_now;
		
		$corner_name = stripslashes_deep($_POST['corner_name']);
		$corner_width = multiple_inputs('corner_width');
		$corner_table_sizes = multiple_inputs('corner_table_sizes');
		$corner_table_pricesBase = multiple_inputs('corner_table_pricesBase');
		$corner_table_pricesAnother = multiple_inputs('corner_table_pricesAnother');

			$wpdb->update
					(
						$table_products,  
						array( 'name' => $corner_name, 'photo_url' => $corner_photo_url,
							   'corner_width' => $corner_width, 'corner_table_sizes' => $corner_table_sizes, 
							   'corner_table_pricesBase' => $corner_table_pricesBase, 'corner_table_pricesAnother' => $corner_table_pricesAnother),
						array( 'id' => $corner_list_id ), 
						array( '%s', '%s', '%s', '%s', '%s', '%s'),
						array( '%d' )
					);
					
		redirect('corner', 'list');
		
		
    }
	
	//Удаление товара
	if ( isset($_POST['corner_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corner_list_setup_form');
        }
		  
		$corner_list_id = $_POST['corner_list_id'];

		$corner_photo_url_now = url2path(home_url().($wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$corner_list_id)));
        unlink($corner_photo_url_now);
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $corner_list_id"); 
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации

		$corner_width_base = explode(", ", $item->corner_width);
		
		$corner_table_sizes = explode(", ", $item->corner_table_sizes);
		$corner_table_pricesBase = explode(", ", $item->corner_table_pricesBase);
		$corner_table_pricesAnother = explode(", ", $item->corner_table_pricesAnother);
		
		$corner_photo_url_now = $item->photo_url;
		
		echo
		"
			<form id='corner_list' class='list_form' name='corner_list_setup' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=corner&amp;tab=corner_list&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('corner_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='corner_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='corner_name' value='".$item->name."' style='width:300px;'/></td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Фото:</td>
					<td><img src='".home_url().$item->photo_url."'/></td>
					<td style='text-align:right;'>Изменить фото:</td>
					<td><input type='file' name='corner_foto' accept='image/*' value='".$item->photo_url."' style='width:300px;'/></td>
					<td><p>Загружайте изображения<br /> только правых сегментов!</p></td>
					<td></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Размеры:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td> 
						<label>Ширина - </label>
						<label>Мин <input id='corner_add_min_width' type='text' name='corner_width[]' style='width:50px;' value='".$corner_width_base[0]."' /></label>
						<label>Макс <input id='corner_add_max_width' type='text' name='corner_width[]' style='width:50px;' value='".$corner_width_base[1]."' /></label>
						<label>Шаг <input id='corner_add_step_width' type='text' name='corner_width[]' style='width:50px;' value='".$corner_width_base[2]."' /></label>
						
						<br />
						<br />
						
						<button class='corner_add_size_table_reload'>Обновить Таблицу цен</button>
						
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Таблица Цен<br/>(заполняется на<br /> основании ширины):</td>
					<td>
					<table class='corner_add_size_table'>
						<tr class='corner_add_raw_sizes'>
							<td class='first_cell'>Размеры</td>";
					
					for($i = 0; $i < count($corner_table_sizes); $i++) {
						echo "<td><input type='text' class='corner_add_input_sizes' name='corner_table_sizes[]' style='width:50px;' value='".$corner_table_sizes[$i]."' /></td>";
					}
					
					echo "</tr>
						<tr class='corner_add_raw_pricesBase'>
							<td class='first_cell'>Цена для базовой высоты</td>";
					
					for($i = 0; $i < count($corner_table_pricesBase); $i++) {
						echo "<td><input type='text' class='corner_add_input_pricesBase' name='corner_table_pricesBase[]' style='width:50px;' value='".$corner_table_pricesBase[$i]."' /></td>";
					}		
							
					echo "</tr>
						<tr class='corner_add_raw_pricesAnother'>
							<td class='first_cell'>Цена высоты, отличной от базовой</td>";
					
					for($i = 0; $i < count($corner_table_pricesAnother); $i++) {
						echo "<td><input type='text' class='corner_add_input_pricesAnother' name='corner_table_pricesAnother[]' style='width:50px;' value='".$corner_table_pricesAnother[$i]."' /></td>";
					}			
					
					echo "</tr>
						
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='corner_add_btn'type='submit' class='button' name='corner_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='corner_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}

function corner_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corner;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['corner_add_btn']) ) 
    {
    
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('corner_add_form');
        }
        
        //Файлы
		if ( isset ($_FILES['corner_foto']) && $_FILES['corner_foto']['name'] != '') {
			$file_name_new = translit($_FILES['corner_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/corner/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/corner/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/corner/", 0755, true);
			}
			
			copy ( $_FILES['corner_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 
		}
		
		  
		$corner_name = stripslashes_deep($_POST['corner_name']);
		$corner_photo_url = '/wp-content/uploads/cupboardcalc/corner/'.$file_name_new;
		$corner_width = multiple_inputs('corner_width');
		$corner_table_sizes = multiple_inputs('corner_table_sizes');
		$corner_table_pricesBase = multiple_inputs('corner_table_pricesBase');
		$corner_table_pricesAnother = multiple_inputs('corner_table_pricesAnother');
			
			$wpdb->insert
					(
						$table_products,  
						array( 'name' => $corner_name, 'photo_url' => $corner_photo_url,
							   'corner_width' => $corner_width, 'corner_table_sizes' => $corner_table_sizes, 
							   'corner_table_pricesBase' => $corner_table_pricesBase, 'corner_table_pricesAnother' => $corner_table_pricesAnother),   
						array( '%s', '%s', '%s', '%s', '%s', '%s')
					);
		
		redirect('corner','add');
    
    }
    
    
	
	//Форма добавления товара
	echo
		"
			
			<form id='corner_add' class='add_form' name='corner_add' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=corner&amp;tab=corner_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('corner_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='corner_name' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Загрузить фото:</td>
					<td><input type='file' name='corner_foto' accept='image/*' style='width:300px;'/></td>
					<td>Загружайте изображения только правых сегментов!</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Размеры:</td>
				</tr>
				<tr>
					<td style='text-align:right;'></td>
					<td> 
						<label>Ширина - </label>
						<label>Мин <input id='corner_add_min_width' type='text' name='corner_width[]' style='width:50px;' /></label>
						<label>Макс <input id='corner_add_max_width' type='text' name='corner_width[]' style='width:50px;' /></label>
						<label>Шаг <input id='corner_add_step_width' type='text' name='corner_width[]' style='width:50px;' /></label>
						
						<br />
						<br />
						
						<button class='corner_add_size_table_reload'>Обновить Таблицу цен</button>
						
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Таблица Цен<br/>(заполняется на<br /> основании ширины):</td>
					<td>
					<table class='corner_add_size_table'>
						<tr class='corner_add_raw_sizes'>
							<td class='first_cell'>Ширина</td>	
						</tr>
						<tr class='corner_add_raw_pricesBase'>
							<td class='first_cell'>Цена для базовой высоты</td>
						</tr>
						<tr class='corner_add_raw_pricesAnother'>
							<td class='first_cell'>Цена для высоты, отличной от базовой</td>
						</tr>
						
					</table>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='corner_add_btn' type='submit' class='button' name='corner_add_btn' value='Добавить' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}

/*
	ДОПОЛНИТЕЛЬНЫЕ ЭЛЕМЕНТЫ!
*/

function additional_options_page() {
	if ( $_GET['page'] == 'additional' && !isset ( $_GET['tab'] )) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=admin.php?page=additional&tab=additional_list">';
		exit;
	}
	
	echo "<h2>Каталог дополнительных элементов</h2><br />";
	
	$tab_pages = array( 'additional_list' => 'Список дополнительных элементов', 'additional_add' => 'Добавить дополнительный элемент');

    //generic HTML and code goes here

    if ( isset ( $_GET['tab'] ) ) cupboard_admin_tabs($tab_pages, $_GET['tab'], 'additional'); else cupboard_admin_tabs($tab_pages, 'additional_list', 'additional');


	if ( $_GET['tab'] == 'additional_list' && $_GET['page'] == 'additional' ){
		additional_list_product();
	} else if ( $_GET['tab'] == 'additional_add' && $_GET['page'] == 'additional' ) {
		additional_add_product();
	}

}

function additional_list_product() {
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_additional;
	
	//Сохранение изменений товаров
	if ( isset($_POST['additional_list_setup_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('additional_list_setup_form');
        }
		  
		//Файлы
		if ( isset ($_FILES['additional_foto']) && $_FILES['additional_foto']['name'] != '') {
			$file_name_new = translit($_FILES['additional_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/additional/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/additional/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/additional/", 0755, true);
			}
			
			copy ( $_FILES['additional_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 

		}
		
		$additional_list_id = $_POST['additional_list_id'];
		
		//проверка фото, в случае несовпадения переписывает
		$additional_photo_url_now = $wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$additional_list_id );
		$additional_photo_url = '/wp-content/uploads/cupboardcalc/additional/'.$file_name_new;
		$file_name_new != null ? $additional_photo_url = $additional_photo_url : $additional_photo_url = $additional_photo_url_now;
		
		$additional_name = stripslashes_deep($_POST['additional_name']);
		$additional_price = stripslashes_deep($_POST['additional_price']);
			
			$wpdb->update
					(
						$table_products,  
						array( 'name' => $additional_name, 'photo_url' => $additional_photo_url,
							   'price' => $additional_price),   
					    array( 'id' => $additional_list_id ), 
						array( '%s', '%s', '%s' ),
						array( '%d' )
					);
					
		redirect('additional', 'list');
		
		
    }
	
	//Удаление товара
	if ( isset($_POST['additional_list_delete_btn']) ) 
    {   
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('additional_list_setup_form');
        }
		  
		$additional_list_id = $_POST['additional_list_id'];

		$additional_photo_url_now = url2path(home_url().($wpdb->get_var( "SELECT photo_url FROM $table_products WHERE id=".$additional_list_id)));
        unlink($additional_photo_url_now);
		
		$wpdb->query("DELETE FROM $table_products WHERE id = $additional_list_id"); 
    }
    
    
    
	//Вывод формы информации по товарам
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
		//расшифровка информации
		$additional_photo_url_now = $item->photo_url;
		
		echo
		"
			<form id='additional_list' class='list_form' name='additional_list_setup' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=additional&amp;tab=additional_list&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('additional_list_setup_form'); 
		}
		
		echo
		"
				<p style='padding-top:30px;'><b>Товар ID = ".$item->id."</b><input type='hidden' name='additional_list_id' value='".$item->id."'/></p>
				<table class='list_table'>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='additional_name' value='".$item->name."' style='width:300px;'/></td>
					<td><input type='button' class='show_list collapsed' value='Показать'></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Фото:</td>
					<td><img src='".home_url().$item->photo_url."'/></td>
					<td style='text-align:right;'>Изменить фото:</td>
					<td><input type='file' name='additional_foto' accept='image/*' value='".$item->photo_url."' style='width:300px;'/></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Цена:</td>
					<td><input type='text' name='additional_price' value='".$item->price."' style='width:150px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='additional_add_btn'type='submit' class='button' name='additional_list_setup_btn' value='Сохранить' />
						<input type='submit' class='button' name='additional_list_delete_btn' value='Удалить' />
 					</td>
				</tr>
			</table>
			</form>
		";
	}
}

function additional_add_product()
{
	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_additional;
	
	
	//Сохранение добавленного товара в базу
	if ( isset($_POST['additional_add_btn']) ) 
    {
    
    
       if (function_exists('current_user_can') && 
            !current_user_can('edit_others_posts') )
                die ( _e('Hacker?', 'cupboard') );

        if (function_exists ('check_admin_referer') )
        {
            check_admin_referer('additional_add_form');
        }
        
        //Файлы
		if ( isset ($_FILES['additional_foto']) && $_FILES['additional_foto']['name'] != '') {
			$file_name_new = translit($_FILES['additional_foto']['name']);
			$file_name_new_full = WP_CONTENT_DIR."/uploads/cupboardcalc/additional/".$file_name_new;
			
			if (!file_exists(WP_CONTENT_DIR."/uploads/cupboardcalc/additional/")) {
				mkdir(WP_CONTENT_DIR."/uploads/cupboardcalc/additional/", 0755, true);
			}
			
			copy ( $_FILES['additional_foto']['tmp_name'], $file_name_new_full);

			$fp = fopen($file_name_new_full, "rb");
			$file = fread($fp,filesize($file_name_new_full));
			fclose($fp); 
		}
		
		
		  
		$additional_name = stripslashes_deep($_POST['additional_name']);
		$additional_photo_url = '/wp-content/uploads/cupboardcalc/additional/'.$file_name_new;
		$additional_price = stripslashes_deep($_POST['additional_price']);
			
		$wpdb->insert
				(
					$table_products,  
					array( 'name' => $additional_name, 'photo_url' => $additional_photo_url,
						   'price' => $additional_price),   
					array( '%s', '%s', '%s' )
				);
					

		
		//redirect('additional','add');
    
    }
    
    
	
	//Форма добавления товара
	echo
		"
			
			<form id='additional_add' class='add_form' name='additional_add' enctype='multipart/form-data' method='post' action='".$_SERVER['PHP_SELF']."?page=additional&amp;tab=additional_add&amp;updated=true'>
			<h3 class='form_error'></h3>
			<h3 class='form_table_error'></h3>
		";
		
		if (function_exists ('wp_nonce_field') )
		{
			wp_nonce_field('additional_add_form'); 
		}
	
	echo
	"
			<table>
				<tr>
					<td style='text-align:right;'>Название:</td>
					<td><input type='text' name='additional_name' style='width:300px;'/></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Загрузить фото:</td>
					<td><input type='file' name='additional_foto' accept='image/*' style='width:300px;'/></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td style='text-align:right;'>Цена за 1 шт:</td>
					<td><input id='additional_price' type='text' name='additional_price' style='width:150px;' /></td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<input id='additional_add_btn' type='submit' class='button' name='additional_add_btn' value='Добавить' />
 					</td>
				</tr>
			</table>
			
		</form>
	";
}


//Tabs

function cupboard_admin_tabs( $pages, $current, $section) {
    $tabs = $pages;
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=$section&tab=$tab'>$name</a>";

    }
    echo '</h2>';
}



function my_admin_notice() {
    ?>
    <div class="updated">
        <p><?php _e( 'Информация изменена!', 'my-text-domain' ); ?></p>
    </div>
    <?php
}




add_action('admin_menu', 'cupboard_add_admin_page');
?>