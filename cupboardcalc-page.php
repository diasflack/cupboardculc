<?php


/* Styles and scripts */

function cupboard_admin_styles() {
	wp_register_style( 'jquery-ui', plugins_url( '/css/jquery-ui.css', __FILE__ ), array(), '1', 'all' );
	wp_register_style( 'cupboard-style', plugins_url( '/css/cupboard-style-page.css', __FILE__ ), array(), '1', 'all' );

	wp_enqueue_style( 'cupboard-style' );
	wp_enqueue_style( 'jquery-ui' );

}

function cupboard_admin_script() {
	wp_register_script( 'html2canvas', plugins_url( '/js/html2canvas.js', __FILE__ ), array('jquery'), '1.0', true );
	wp_register_script( 'cupboard-script', plugins_url( '/js/cupboard-script-page.js', __FILE__ ), array('jquery','html2canvas'), '1.0', true );
	wp_register_script( 'jquery-mobile', plugins_url( '/js/jquery.ui.touch-punch.min.js', __FILE__ ), array('jquery'), '1.0', true );

	wp_enqueue_script("jquery-ui-core", array('jquery'));
	wp_enqueue_script("jquery-ui-draggable", array('jquery','jquery-ui-core'));
	wp_enqueue_script("jquery-ui-droppable", array('jquery','jquery-ui-core'));
	wp_enqueue_script("jquery-ui-resizable", array('jquery','jquery-ui-core'));

	wp_enqueue_script('html2canvas');
	wp_enqueue_script('cupboard-script');
	wp_enqueue_script("jquery-mobile", array('jquery','jquery-ui-core'));

	wp_localize_script( 'cupboard-script', 'ajax_object',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) );
}

add_action('init', 'init_theme_method');

function init_theme_method() {
	add_thickbox();
}


/* Main function */

function cupboard_calc() {

	cupboard_admin_styles();
	cupboard_admin_script();
	cupboard_calc_page();

}

/* Main page html */

function cupboard_calc_page() {
	?>
	<div class="image_overlay">

	</div>

	<div class="image_overlay_descr">
		<p><button id='rotateImage' class="btn-yellow">Развернуть изображение</button> <em>Измените размер и положение заполнения и нажмите принять </em> <button id='acceptImage' class="btn-yellow">Принять</button></p>

	</div>

	<div id='cupboard_calc_page' >

		<?php //Шаблон?>

    <div id='cupboard_calc' class="container-safeguards calculator-page">

		<div class="row ">

			<div id='city_choice' class="col-sm-3">
				<?php
				$cupboard_cities_city = explode(", ", get_option('cupboard_cities_city'));
				$cupboard_cities_multi = explode(", ", get_option('cupboard_cities_multi'));
				?>
				<select id='city_price' class="ml-select calculator-town" style="width: 212px;">
					<option value=''>Выберите город</option>
					<?php
					for($i = 0; $i < count($cupboard_cities_city); $i++) {
						echo "<option value='".$cupboard_cities_multi[$i]."'>".$cupboard_cities_city[$i]."</option>";
					}
					?>
				</select>
			</div>

			<div id='cupboard_calc_sizes'>
				<div class='cupboard_page_sizes'>

					<?php cupboard_corpus_sizes(); ?>

					<div class="col-sm-3 ml-calc-dimensions-small">
						<button id='cupboard_page_search_corpuses' class="btn-yellow">
							Подобрать
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-9 right">


				<div id="corpus" class="calc-fur-type scroll-panel" tabindex="0">
					<div id="boxscroll" class="cupboard_page_corpuses scroll-content"></div>

				</div>



				<div class="ml-calc-signed ml-calc-signed-bolder cupboard_page_doors">
					<div class="ml-calc-sign">
						Количество дверей
					</div>
					<div><select class='cupboard_page_doors_radio left ml-select number-door'> </select></div>
				</div>

				<div id="ugl" class='l-calc-signed ml-calc-signed-bolder cupboard_page_corners'>
					<label><div class="ml-calc-sign">Угловые сегменты</div>

						<input id='cupboard_page_corners_check' type='checkbox' name='corners_check' class='left' />
						<p class='left'><small><em>нажмите, если хотите добавить угловые сегменты</em></small></p></label>

					<div class='cupboard_page_corners_choose clear cf'>

						<div id="cupboard_page_corners_check" class="ml-choose-h">Выберите необходимые угловые сегменты:</div>
						<div class="block-check">
							<div class='cupboard_page_corners_left'>
								<label class='left cupboard_page_corners_label ml-choose-left'><em>Левый </em><input class='cupboard_page_corners_main_checkbox' id='cupboard_page_corners_left' type='radio' name='corners' /></label>
								<div id="carousel-left" class='carousel cupboard_page_corners_chosen left'>
									<div id="carus1" class=' scroll-img'>
										<ul class='cupboard_page_corners_images'>
										</ul>
									</div>
									<label>Ширина - <select id='cupboard_page_corner_width_range_left' class='cupboard_page_corners_range'></select> мм</label>
									<div id="carus1-btn" class="text-center">
										<button class="btn carousel-control carousel-control-right" id="carus1-backward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-right-y.png" alt=""></span></button>
										<button class="btn carousel-control carousel-control-left" id="carus1-forward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-left-y.png" alt=""></span></button>

									</div>				</div>
							</div>
							<div class='clear'></div>


							<div class='cupboard_page_corners_right clear'>
								<label class='left cupboard_page_corners_label ml-choose-right'><em>Правый </em><input class='cupboard_page_corners_main_checkbox' id='cupboard_page_corners_right' type='radio' name='corners' /></label>
								<div id="carousel-right" class='carousel cupboard_page_corners_chosen left'>
									<div id="carus2" class=' scroll-img'>
										<ul class='cupboard_page_corners_images'>
										</ul>
									</div>
									<label>Ширина - <select id='cupboard_page_corner_width_range_right' class='cupboard_page_corners_range'></select> мм</label>
									<div id="carus2-btn" class="text-center">
										<button class="btn carousel-control carousel-control-left" id="carus2-backward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-right-y.png" alt=""></span></button>
										<button class="btn carousel-control carousel-control-right" id="carus2-forward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-left-y.png" alt=""></span></button>
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class='clear'></div>
				</div>


				<div id='cupboard_full_cost'>
					<input type='hidden' id='city_cost' value="0"/>
					<input type='hidden' id='corpus_cost' value="0.00"/>
					<input type='hidden' id='corner_cost_left' value="0.00"/>
					<input type='hidden' id='corner_cost_right' value="0.00"/>
					<input type='hidden' id='additional_cost' value="0.00"/>
					<input type='hidden' id='profile_cost' value="0.00"/>
					<input type='hidden' id='frame_cost' value="0.00"/>
					<input type='hidden' id='image_cost' value="0.00"/>

					<?php if (get_option('cupboard_discount_enable')) { ?>			<input type='hidden' id='cupboard_discount_value_top' value='<?php echo get_option('cupboard_discount_value') ?>'/>
					<?php } ?>

				</div>

				<div id="dop" class="killme480">
					<div class='cupboard_page_additional'>
						<label><div class="ml-calc-sign">Дополнительные элементы</div>

							<input id='cupboard_page_additional_check' type='checkbox' name='additional_check' class='left' />
							<p class='left'><small><em>нажмите, чтобы добавить дополнительные элементы</em></small></p></label>

						<div class='cupboard_page_additional_choose clear cf'>

							<div class="ml-choose-h">Выберите дополнительные элементы и введите их количество:</div>


							<div class='cupboard_page_additional_list left'>
								<div id="carus3" class=' scroll-img'>
									<ul class='cupboard_page_additional_images'>
									</ul>
								</div>
								<div id="carus3-btn" class="text-center">
									<button class="btn carousel-control carousel-control-left" id="carus3-forward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-right-y.png" alt=""></span></button>
									<button class="btn carousel-control carousel-control-right" id="carus3-backward"><span><img src="<?php echo get_template_directory_uri();?>/img/arrow-left-y.png" alt=""></span></button>
								</div>
							</div>

						</div>

						<div class='clear'></div>
					</div>
				</div>


				<div id="rtytrt" class='cupboard_page_laminate'>

					<div class="ml-calc-sign">Ламинат</div>

					<button class='clear btn btn-yellow' id='cupboard_page_search_laminate'>Выбрать ламинат</button>

					<div id="boxscroll1" class='cupboard_page_laminates'>

					</div>

				</div>

				<div id="profile-a" class='cupboard_page_profile'>
					<div class="ml-calc-sign">Профиль</div>
					<button class='clear btn btn-yellow' id='cupboard_page_search_profile'>Выбрать профиль</button>
					<div class='cupboard_page_profile_list'>
						<div class="col-sm-3">
							<div class="profile_div" style="display:none;">
								<p class="ml-choose-h">Выберите вид профиля</p>
								<ul class="ml-list-choose-left-menu"></ul>
							</div>
						</div>
						<div class='profile_options'></div>
						<div class="profile_color" style="display:none;">
							<p class="ml-choose-h">Выберите цвет</p>
							<div id="carus5" class="scroll-img">
								<ul class="profileColorList"></ul>
							</div>
							<div id="carus5-btn" class="text-center">
								<button class="btn carousel-control carousel-control-left" id="carus5-forward">
						<span>
							<img src="/wp-content/themes/mebelite/img/arrow-right-y.png" alt="">
						</span>
								</button>
								<button class="btn carousel-control carousel-control-right" id="carus5-backward">
						<span>
							<img src="/wp-content/themes/mebelite/img/arrow-left-y.png" alt="">
						</span>
								</button>
							</div>
						</div>
					</div>
				</div>

				<div id="frm" class='cupboard_page_frame'>

					<div class="ml-calc-sign">Рамки</div>

					<button class='clear btn btn-yellow' id='cupboard_page_search_frame'>Выбрать рамки</button>

					<div class='cupboard_page_frame_list'>
						<div class="frame_choose_div" style="display:none;">
							<div class="ml-choose-h">Нажмите на рамку, чтобы перенести ее на незанятую область двери на изображении ниже</div>
							<div class="scroll-img frame_list cf">
								<ul class="frameList">
								</ul>
							</div>
						</div>

					</div>

					<div class='cupboard_page_image'>

						<div class="ml-calc-sign">Заполнения</div>



						<div class='cupboard_page_image_list'>

							<div class="ml-choose-h">нажмите на разноцветные блоки, чтобы выбрать желаемое заполнение</div>

							<div class='cupboard_model'>
								<!--	<a href="#TB_inline?height=600&amp;width=750&amp;inlineId=image_popup" class="thickbox"><button id="frame_image_submit">Выбрать заполнение</button></a>
                                    <div class='doors_div not_active_door' style = 'width:122px; height:202px;'></div>
                                    <div class='doors_div not_active_door' style = 'width:122px; height:202px;'></div>
                                    <div class='doors_div not_active_door' style = 'width:122px; height:202px;'></div> -->

							</div>

							<div id="image_popup" style="display:none">
								<?php show_images(); ?>

							</div>

						</div>

					</div>



					<div class='cupboard_page_complete'>

						<div class='installment'>

							<a class="btn-white btn-inline"><input type='checkbox' id='installment'> Покупка в рассрочку?</label></a>

						</div>

						<button class='btn-yellow btn-inline"' id='cupboard_page_complete'>ГОТОВО</button>

						<p class='error_complete error_corpus'>Выберите корпус!</p>
						<p class='error_complete error_laminate'>Выберите ламинат!</p>
						<p class='error_complete error_profile_handler'>Выберите тип профиля!</p>
						<p class='error_complete error_profile'>Выберите цвет профиля!</p>
						<p class='error_complete error_frame'>Перенесите рамки на все области двери!</p>
						<p class='error_complete error_image'>Заполните все блоки рамок!</p>


					</div>
				</div>
			</div>
			<!-- Sidebar -->
			<div class="col-sm-3 killme480" style="position:relative;">
				<div id="test" class="affixxx">

					<div id="navbar">
						<ul class="nav">
							<li class="crps"><a href="#corpus" class="ml-select-btn">Корпус шкафа</a></li>
							<li class="ugl "><a href="#ugl" class="ml-select-btn ml-select-btn-active">Угловой элемент</a></li>
							<li class="dop "><a href="#dop" class="ml-select-btn">Доп. элементы</a></li>
							<li class="rtytrt"><a href="#rtytrt" class="ml-select-btn">Ламинат</a></li>
							<li class="profile-a"><a href="#profile-a" class="ml-select-btn">Профиль</a></li>
							<li class="frm "><a href="#frm" class="ml-select-btn">Рамки</a></li>
						</ul>
					</div>
					<div id='cost_top' class="calculator-yellow-cont">
						<div class="calculator-yellow-cont-inner-cont">
							<div class="calculator-yellow-cont-headings">
								Цена:
							</div>
							<span id='cost'>0</span> руб.
						</div>
						<?php if (get_option('cupboard_discount_enable')) { ?>
							<div class="calculator-yellow-cont-inner-cont">
								<div id='discount_reason' class="calculator-yellow-cont-headings">
									<span>Причина скидки:</span><br>
									<span style="color: #2b2b2b;font-size: 16px;font-weight: bold;"><?php echo get_option('cupboard_discount_reason') ?> </span>
								</div>

							</div>
							<div>
								<div id='discount_top' class="calculator-yellow-cont-headings">
									<span>Величина скидки:</span><br>
									<span style="color: #2b2b2b;font-size: 16px;font-weight: bold;"><?php echo get_option('cupboard_discount_value') ?> % </span>
								</div>

							</div>
						<?php } ?>
					</div>
					<p class="mob-p text-center">В полной версии калькулятора, доступной на компьютерах  и планшетах  вы сможете посчитать стоимость шкафы с выбранным вами оформлением. </p>
					<p class="mob-p text-center">Вырианты оформление дверей  смотритев разделе<br><a class="link-in-text" href="/category/catalog">Декор и наполнение</a></p>
				</div>
			</div>

		</div>

    </div>

		<div id='cupboard_result'>

			<form>
				<div class='cupboard_client_result'>
					<div>
						<label>Город - <br><span id='clientCity'></span></label><br>
						<label>Заказчик <br><input type='text' id='clientName' name='clientName'></label> <span class='error_complete' id='clientNameError'><br>Введите ваше имя!</span>
					</div>
					<div>
						<label>Телефон <br><input type='text' id='clientTel' name='clientTel'> <span class='error_complete' id='clientTelError'><br>Введите ваш телефон!</span></label><br>
						<label>E-mail <br><input type='text' id='clientMail' name='clientMail'></label>
					</div>
					<div>
						<label>Менеджер <br><input type='text' id='managerName' name='managerName'></label>
					</div>
				</div>

				<div class='cupboard_corpus_result cf'>

					<img class='left' src='' alt='корпус' />

					<div class='descr left'>

						<p>Артикул - <span id='corpus_articul_result'></span></p>
						<p>Ширина корпуса - <span id='corpus_width_result'></span></p>
						<p>Высота корпуса - <span id='corpus_height_result'></span></p>
						<p>Глубина корпуса - <span id='corpus_depth_result'></span></p>
						<p>Цвет корпуса - <span id='laminate_result'></span></p>

						<p>Количество дверей - <span id='corpus_doors_result'></span></p>

					</div>

				</div>

				<div class='cupboard_info_result cf'>

					<p>Задняя стенка шкафа: ДВПО Белое</p>
					<p>Кромка шкафа наружняя: ПВХ 2мм</p>
					<p>Кромка шкафа внутренняя: ПВХ 0,4мм</p>
					<p>Крепеж шкафа: Grandis мал+евровинт</p>
					<p>Заглушки: Пластик или самоклеющиеся</p>
					<p>Ручки для ящиков: Хром</p>

				</div>

				<div class='cupboard_corners_result cf'>

					<div class='left'>
						<p>Левый угловой сегмент - <span id='corner_name_left_result'></p>
						<p>Ширина - <span id='corner_left_result'></span></p>
					</div>

					<div class='right'>
						<p>Правый угловой сегмент - <span id='corner_name_right_result'></p>
						<p>Ширина - <span id='corner_right_result'></span></p>
					</div>

				</div>

				<div class='cupboard_additional_result cf'>

				</div>

				<div class='cupboard_profile_result cf'>
					<p>Название профиля - Grandis</p>
					<p>Вид профиля - <span id='profile_type_result'></span></p>
					<p>Цвет профиля - <span id='profile_color_result'></span></p>
					<p>Тип ручки - <span id='profile_handle_result'></span></p>
				</div>

				<div class='cupboard_frame_result cf'>

					<div class='cupboard_model_result cf'>

					</div>

				</div>

				<div class='cupboard_cost_result cf'>
					<p>Стоимость - <span id='cost_result'></span> руб.</p>
					<p>Рассрочка - <span id='installment_result'></span></p>
				</div>

				<div class='cupboard_manager_result cf'>
					<p>Договор № - <span id='contract_underline'></span> Сумма изделия: <span id='manager_cost_result'></span> руб.</p>
					<p>С размерами и наполнением дверей<br/> шкафа согласен(подпись) - <span id='sign_underline'></span></p>

					<br/>

					<button class='left btn btn-yellow' id='cupboard_back'>Назад</button><button class='right btn btn-yellow' id='cupboard_print'>Распечатать</button><button class='right btn btn-yellow' id='cupboard_send'>Отправить заявку</button>
				</div>

			</form>

		</div>
		<p class='success'>Заявка успешно отправлена!</p>
	</div>



	<?php

}

/* Corpuses block */
function range_to_string($min,$max,$step){

	if ($step == 0) $step = 1;

	$range = array();

	for ($i=$min; $i <= $max; $i = $i + $step) {
		array_push($range, $i);
	}

	return $range;

}

function not_in_array($main_array, $check_array) {

	for ($i=0; $i<count($check_array); $i++) {

		if (!in_array($check_array[$i], $main_array)) {
			array_push($main_array, $check_array[$i]);
		}

	}

	return $main_array;

}

function cupboard_corpus_sizes() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corpuses;

	$i = 0;
	$corpus_width = array();
	$corpus_height = array();
	$corpus_depth = array();

	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item)
	{

		$corpus_height_base = explode(", ", $item->corpus_height);
		$corpus_width_base = explode(", ", $item->corpus_width);
		$corpus_depth_base = explode(", ", $item->corpus_depth);

		//Определение вхождения в диапазон размеров

		$corpus_height_range = range_to_string($corpus_height_base[0],$corpus_height_base[1],$corpus_height_base[2]);
		$corpus_width_range = range_to_string($corpus_width_base[0],$corpus_width_base[1],$corpus_width_base[2]);
		$corpus_depth_range = range_to_string($corpus_depth_base[0],$corpus_depth_base[1],$corpus_depth_base[2]);

		$corpus_width = not_in_array($corpus_width, $corpus_width_range);
		$corpus_height = not_in_array($corpus_height, $corpus_height_range);
		$corpus_depth = not_in_array($corpus_depth, $corpus_depth_range);

		print_r($corpus_width_depth);

	}

	sort($corpus_width);
	sort($corpus_height);
	sort($corpus_depth);



	echo "<div class='col-sm-2 ml-calc-dimensions-small'><div class='ml-calc-signed'><div class='ml-calc-sign'><p>Ширина мм</p></div><select id='cupboard_page_width_range' class='ml-select ml-select-top'>";

	for ($i=0; $i<count($corpus_width); $i++) {

		if ($corpus_width[$i]==2400) {
			echo "<option selected>".$corpus_width[$i]."</option>";

		} else echo "<option>".$corpus_width[$i]."</option>";

	}
	echo "<select /></div></div>";
	echo "<div class='col-sm-2 ml-calc-dimensions-small'><div class='ml-calc-signed'><div class='ml-calc-sign'><p>Высота мм</p></div><select id='cupboard_page_height_range' class='ml-select ml-select-top'>";

	for ($i=0; $i<count($corpus_height); $i++) {

		if ($corpus_height[$i]===2400) {
			echo "<option selected='selected'>".$corpus_height[$i]."</option>";

		} else echo "<option>".$corpus_height[$i]."</option>";

	}
	echo "<select /></div></div>";
	echo "<div class='col-sm-2 ml-calc-dimensions-small'><div class='ml-calc-signed'><div class='ml-calc-sign'><p>Глубина мм</p></div><select id='cupboard_page_depth_range' class='ml-select ml-select-top'>";

	for ($i=0; $i<count($corpus_depth); $i++) {

		if ($corpus_depth[$i]==='600') {
			echo "<option selected='selected'>".$corpus_depth[$i]."</option>";
		} else echo "<option>".$corpus_depth[$i]."</option>";

	}
	echo "<select /></div></div>";


	/*			<strong><span class='cupboard_page_width_range'>2100</span> мм<br/>
                <span class='cupboard_page_height_range'>2100</span> мм<br/>
                <span class='cupboard_page_depth_range'>600</span> мм<br/></strong>

                <input class='cupboard_page_range' type='range' id='cupboard_page_width_range' min='1200' max='3000' step='50'/><br/>
                <input class='cupboard_page_range' type='range' id='cupboard_page_height_range'  min='1200' max='3000' step='50'/><br/>
                <input class='cupboard_page_range' type='range' id='cupboard_page_depth_range' min='450' max='600' step='150'/><br/>

                        */
}

//True image url function
function true_images_url($images_url) {

	if ($images_url !== "") {

		$images_url = explode(",",$images_url);

		for($a = 0; $a < count($images_url); $a++) {
			$images_url[$a] = home_url().substr($images_url[$a], strpos($images_url[$a],"/wp-content/"), strlen($images_url[$a]));
		}

		$images_url = implode(",", $images_url);

	}

	return $images_url;
}
/* Images block */
function show_images() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_image;

	$products = $wpdb->get_results("SELECT * FROM $table_products");

	echo "<div class='left image_categories'>";

	foreach ($products as $item)
	{

		//расшифровка информации

		$image_array = json_decode($item->image_array, true);
		$image_cost = json_decode($item->image_cost, true);

		$image_array[0]['image_url'] = true_images_url($image_array[0]['image_url']);

		echo "
			<ul>
				<li class='main_category_li'><a class='main_category'>".$image_array[0]['image_name']."</a>
				<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' value='".$image_array[0]['image_url']."'/>
				<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' value='".$image_array[0]['image_caption']."'/>
				<input type='hidden' name='imageCost' class='imageCost' value='".end($image_cost)."'/>
				<input type='hidden' name='fillMethod' class='fillMethod' value='".$item->image_fillMethod."'/></li>
				";



		for($i = 1; $i < count($image_array); $i++) {


			if ($image_array[$i]['image_sub'] == 'subcategory') {
				$category_imageGallery = 'subcategory_imageGallery';
				$li_class = 'subcategory_li';
				$last_price = $image_cost[$i-1];

				$image_cost[$i-1] != 0 ? $image_cost_ad = $image_cost[$i-1] : $image_cost_ad = end($image_cost);

			} else {
				$category_imageGallery = 'subsubcategory_imageGallery';
				$li_class = 'subsubcategory_li';

				if ($image_cost[$i-1] != 0) {
					$image_cost_ad = $image_cost[$i-1];
				} elseif ($last_price != 0) {
					$image_cost_ad = $last_price;
				} else {
					$image_cost_ad = end($image_cost);
				}

			}

			$image_array[$i]['image_url'] = true_images_url($image_array[$i]['image_url']);


			echo "
							
							

							<li class='".$li_class."'><a class='".$category_imageGallery."'>".$image_array[$i]['image_name']."</a>
							<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' value='".$image_array[$i]['image_url']."'/>
							<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' value='".$image_array[$i]['image_caption']."'/>
							<input type='hidden' name='imageCost' class='imageCost' value='".$image_cost_ad."'/>
							<input type='hidden' name='fillMethod' class='fillMethod' value='".$item->image_fillMethod."'/></li>";

		}

		echo "</li></ul>";

	}

	echo "</div><div class='left load_images'></div>";

}

?>