<?php
add_action( 'wp_ajax_corpus_values', 'corpus_values_callback' );
add_action( 'wp_ajax_nopriv_corpus_values', 'corpus_values_callback' );
add_action( 'wp_ajax_corners_values', 'corners_values_callback' );
add_action( 'wp_ajax_nopriv_corners_values', 'corners_values_callback' );
add_action( 'wp_ajax_additional_values', 'additional_values_callback' );
add_action( 'wp_ajax_nopriv_additional_values', 'additional_values_callback' );
add_action( 'wp_ajax_laminate_values', 'laminate_values_callback' );
add_action( 'wp_ajax_nopriv_laminate_values', 'laminate_values_callback' );
add_action( 'wp_ajax_profile_values', 'profile_values_callback' );
add_action( 'wp_ajax_nopriv_profile_values', 'profile_values_callback' );
add_action( 'wp_ajax_frame_values', 'frame_values_callback' );
add_action( 'wp_ajax_nopriv_frame_values', 'frame_values_callback' );
add_action( 'wp_ajax_send_mail', 'send_mail_callback' );
add_action( 'wp_ajax_nopriv_send_mail', 'send_mail_callback' );
add_action( 'wp_ajax_rotate_image', 'rotate_image_callback' );
add_action( 'wp_ajax_nopriv_rotate_image', 'rotate_image_callback' );

include(plugin_dir_path( __FILE__ ) ."library/mpdf57/mpdf.php");
/*
	CORPUS FUNCTIONS
*/

function corpus_values_callback() {
	global $wpdb;
	$corpus_height = intval( $_POST['corpus_height'] );
	$corpus_width = intval( $_POST['corpus_width'] );
	$corpus_depth = intval( $_POST['corpus_depth'] );

    cupboard_calc_corpuses($corpus_width,$corpus_height,$corpus_depth);
	die();
}

function range_to_string($min,$max,$step){
	
	$range = array();
	
	if ($step == 0) {
		$step = 1;
	}
	
	for ($i=$min; $i <= $max; $i = $i + $step) {
		array_push($range, $i);
	}
	
	return $range;
	
}


function cupboard_calc_corpuses($corpus_width,$corpus_height,$corpus_depth) {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corpuses;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		$corpus_name = $item->name;
		$corpus_photo_url = $item->photo_url;
		
		$corpus_doors_base =explode(", ", $item->corpus_doors);
		$corpus_height_base = explode(", ", $item->corpus_height);
		$corpus_width_base = explode(", ", $item->corpus_width);
		$corpus_depth_base = explode(", ", $item->corpus_depth);
		
		$corpus_table_sizes = explode(", ", $item->corpus_table_sizes);
		$corpus_table_pricesBase = explode(", ", $item->corpus_table_pricesBase);
		$corpus_table_pricesAnother = explode(", ", $item->corpus_table_pricesAnother);
		$corpus_table_articul = explode(", ", $item->corpus_table_articul);
		$corpus_table_doors = explode(", ", $item->corpus_table_doors);
		
		//Определение вхождения в диапазон размеров
		
		
		$corpus_height_range = range_to_string($corpus_height_base[0],$corpus_height_base[1],$corpus_height_base[2]);
		$corpus_width_range = range_to_string($corpus_width_base[0],$corpus_width_base[1],$corpus_width_base[2]);
		$corpus_depth_range = range_to_string($corpus_depth_base[0],$corpus_depth_base[1],$corpus_depth_base[2]);
		
		if (in_array($corpus_height,$corpus_height_range) && in_array($corpus_width,$corpus_width_range) && in_array($corpus_depth,$corpus_depth_range)) {
			
			$key = array_search($corpus_width,$corpus_table_sizes);

			$corpus_table_doors[$key] = preg_replace('/[\[(.)\]]/', '', $corpus_table_doors[$key]);

			$corpus_doors_base = $corpus_table_doors[$key] === '' ? $corpus_doors_base : $corpus_table_doors[$key];
			
			$corpus_array[$i] = array('corpus_name' => $corpus_name,
									 'corpus_url' => home_url().$corpus_photo_url,									  
									 'corpus_doors' => $corpus_doors_base,
									 'corpus_pricesBase' => $corpus_table_pricesBase[$key],
									 'corpus_pricesAnother' => $corpus_table_pricesAnother[$key],
									 'corpus_articul' => $corpus_table_articul[$key]);
									 
			
			$i++;
			
		}
		
		
		
	}
	
	$corpus_array = json_encode($corpus_array);
	
	$corpus_array = (string)$corpus_array;
	
	echo $corpus_array;
		
}

/*
	CORNERS FUNCTIONS
*/

function corners_values_callback() {
	global $wpdb;

    cupboard_calc_corners();
    
	die();
}

function cupboard_calc_corners() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_corner;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		$corner_name = $item->name;
		$corner_photo_url = $item->photo_url;
		
		$corner_width_base = explode(", ", $item->corner_width);
		$corner_table_sizes = explode(", ", $item->corner_table_sizes);
		$corner_table_pricesBase = explode(", ", $item->corner_table_pricesBase);
		$corner_table_pricesAnother = explode(", ", $item->corner_table_pricesAnother);
		
			
			$corner_array[$i] = array('corner_name' => $corner_name,
									 'corner_url' => home_url().$corner_photo_url,									  
									 'corner_width' => $corner_width_base,
									 'corner_table_sizes' => $corner_table_sizes,
									 'corner_table_pricesBase' => $corner_table_pricesBase,
									 'corner_table_pricesAnother' => $corner_table_pricesAnother);
									 
			
			$i++;
			
		
	}
	
	$corner_array = json_encode($corner_array);
	
	$corner_array = (string)$corner_array;
	
	echo $corner_array;
		
}

/*
	ADDITIONAL FUNCTIONS
*/

function additional_values_callback() {
	global $wpdb;

    $table_products = $wpdb->prefix.cupboard_additional;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		$additional_name = $item->name;
		$additional_photo_url = $item->photo_url;
		$additional_price = $item->price;
			
		$additional_array[$i] = array('additional_name' => $additional_name,
								 'additional_url' => home_url().$additional_photo_url,									  
								 'additional_price' => $additional_price);
								 
		$i++;
			
		
	}
	
	$additional_array = json_encode($additional_array);
	
	$additional_array = (string)$additional_array;
	
	echo $additional_array;
    
	die();
}

/*
	LAMINATE FUNCTIONS
*/

function laminate_values_callback() {

    cupboard_calc_laminate();
    
	die();
}

function cupboard_calc_laminate() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_laminate;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		$laminate_name = $item->name;
		$laminate_photo_url = home_url().$item->photo_url;
			
			$laminate_array[$i] = array('laminate_name' => $laminate_name,
									 'laminate_url' => $laminate_photo_url);
									 
			
			$i++;
			

		
		
		
	}
	
	$laminate_array = (string)json_encode($laminate_array);
	
	echo $laminate_array;
		
}

/*
	PROFILE FUNCTIONS
*/

function profile_values_callback() {

    cupboard_calc_profile();
    
	die();
}

function cupboard_calc_profile() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_profile;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
	
		//расшифровка информации
		$profile_name = $item->profile_name;
		$profile_panels = explode(", ", $item->profile_panels);
		$profile_color = explode(", ", $item->profile_color);
		$profile_color_url = explode(", ", $item->profile_color_url);
		$profile_type = explode(", ", $item->profile_type);
		$profile_kind = explode(", ", $item->profile_kind);
		$profile_type_url = explode(", ", $item->profile_type_url);
		$profile_table_colorType = explode(", ", $item->profile_table_colorType);
		$profile_table_topFrame = explode(", ", $item->profile_table_topFrame);
		$profile_table_bottomFrame = explode(", ", $item->profile_table_bottomFrame);
		$profile_table_divider = explode(", ", $item->profile_table_divider);
		$profile_table_topTrack = explode(", ", $item->profile_table_topTrack);
		$profile_table_bottomTrack = explode(", ", $item->profile_table_bottomTrack);
		$profile_table_handle = explode(", ", $item->profile_table_handle);
		$home_url = home_url();
			
			$profile_array[$i] = array( 'profile_name' => $profile_name, 'profile_panels' => $profile_panels, 'profile_color' => $profile_color, 'profile_color_url' => $profile_color_url,
					'profile_type' => $profile_type, 'profile_kind' => $profile_kind, 'profile_type_url' => $profile_type_url, 'profile_table_colorType' => $profile_table_colorType, 'profile_table_topFrame' => $profile_table_topFrame,
					'profile_table_bottomFrame' => $profile_table_bottomFrame, 'profile_table_divider' => $profile_table_divider, 'profile_table_topTrack' => $profile_table_topTrack, 
					'profile_table_bottomTrack' => $profile_table_bottomTrack, 'profile_table_handle' => $profile_table_handle, 'home_url' => $home_url);
									 
			
			$i++;
	
	}
	
	$profile_array = (string)json_encode($profile_array);
	
	echo $profile_array;
		
}

/*
	FRAME FUNCTIONS
*/

function frame_values_callback() {

    cupboard_calc_frame();
    
	die();
}

function cupboard_calc_frame() {

	global $wpdb;
	$table_products = $wpdb->prefix.cupboard_frame;
	
	$i = 0;
	
	$products = $wpdb->get_results("SELECT * FROM $table_products");
	foreach ($products as $item) 	
	{
			
		$frame_array[$i] = array( 'frame_name' => $item->frame_name, 'frame_array' => json_decode($item->frame_array, true));
		
		$i++;

	}
	
	$frame_array = (string)json_encode($frame_array);
	
	echo $frame_array;
		
}

/*
	SEND_MAIL FUNCTIONS
*/

function send_mail_callback() {
	
	//frames attachment
	$images = $_POST['frames'];
	$atachments = array();

	for ($i = 0, $l = count($images); $i < $l; $i++) {
		$prefix = "Dver_".($i+1)."__";
		$img = $images[$i];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = WP_CONTENT_DIR . "/uploads/cupboardcalc/canvas/" . uniqid($prefix) . '.png';
		$success = file_put_contents($file, $data);
		array_push($atachments, $file);
		//print $success ? $file : 'Unable to save the file.';
	}
	
	//pdf create
	$cupboard_result = stripslashes($_POST['cupboard_result']);
	
	$pdf = WP_CONTENT_DIR.'/uploads/cupboardcalc/order.pdf';
	
	$mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
	$mpdf->charset_in = 'utf-8';

	$stylesheet = file_get_contents(plugin_dir_path( __FILE__ ) .'/css/cupboard-style-page.css');
	$mpdf->WriteHTML($stylesheet, 1);

	//$mpdf->list_indent_first_level = 0; 
	$mpdf->WriteHTML($cupboard_result, 2); 
	$mpdf->Output($pdf, 'F');
	
	array_push($atachments, $pdf);
	
	$headers = 'From: Заказ с сайта' . "\r\n";
	
	//mail send
	$email = get_option('cupboard_order_mail');
	
	add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));

	wp_mail( $email, 'Заказ шкафа - mebelite', 'Смотрите файл во вложении', '', $atachments);
	
	array_map('unlink', $atachments);
	
}

/*
	ROTATE_IMAGE FUNCTIONS
*/
function imgFlip ( $imgsrc, $mode )
{
    $width                        =    imagesx ( $imgsrc );
    $height                       =    imagesy ( $imgsrc );

    $src_x                        =    0;
    $src_y                        =    0;
    $src_width                    =    $width;
    $src_height                   =    $height;

    switch ( $mode )
    {

        case '1': //vertical
            $src_y                =    $height -1;
            $src_height           =    -$height;
        break;

        case '2': //horizontal
            $src_x                =    $width -1;
            $src_width            =    -$width;
        break;

        case '3': //both
            $src_x                =    $width -1;
            $src_y                =    $height -1;
            $src_width            =    -$width;
            $src_height           =    -$height;
        break;

        default:
            return $imgsrc;

    }
    $imgdest                    =    imagecreatetruecolor ( $width, $height );
    if ( imagecopyresampled ( $imgdest, $imgsrc, 0, 0, $src_x, $src_y , $width, $height, $src_width, $src_height ) )
    {
        return $imgdest;
    }
    return $imgsrc;
}

function rotate_image_callback() {

    $img = $_POST['image'];
    $rotated = $_POST['rotated'];

    if ($rotated === 'false') {

    $image=imagecreatefromjpeg($img);

    $rotatedImg = imgFlip($image, '2');
    header('Content-type: image/jpeg');

    $img=str_replace('.jpg', '', $img);

    $img = url2path($img.'-rotated.jpg');

    imagejpeg($rotatedImg, $img);
    imagedestroy($rotatedImg);

    $img=str_replace(ABSPATH, '', $img);

    }

    echo $img;
    die();
}

?>