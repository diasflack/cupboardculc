<?php
/*
Plugin Name: Cupboard Calc
Plugin URI: http://www.cleverleafs.com/skaf/
Description: Калькулятор шкафов
Version: 1.0.0
Author: Dias Flack
Author URI: http://www.cleverleafs.com
*/



/* Admin */
if (is_admin()) {

	if(!function_exists('wp_get_current_user')) {
   		include(ABSPATH . "wp-includes/pluggable.php"); 
	}
	
	//Check user
	get_currentuserinfo();
	$user_current = $current_user->user_login;
	$user_delegate = get_option('cupboard_user_delegate');	
	
	if ($user_current == 'admin' || $user_current == $user_delegate) {
		require(plugin_dir_path( __FILE__ ) . '/cupboardcalc-admin.php');
		
		add_action('admin_menu', 'cupboard_add_admin_page');
	}
	
	//AJAX
	require(plugin_dir_path( __FILE__ ) . '/cupboardcalc-ajax.php');
	
}

/* Shortcode page */

if (!is_admin()) {
	
	require(plugin_dir_path( __FILE__ ) . '/cupboardcalc-page.php');
		
	add_shortcode('cupboard_calc', 'cupboard_calc');
	
}

//Install database

function cupboardcalc_install()
{
    global $wpdb;
	
	$table_products = $wpdb->prefix.cupboard_corpuses;
	$table_products2 = $wpdb->prefix.cupboard_laminate;
	$table_products3 = $wpdb->prefix.cupboard_profile;
	$table_products4 = $wpdb->prefix.cupboard_frame;
	$table_products5 = $wpdb->prefix.cupboard_image;
	$table_products6 = $wpdb->prefix.cupboard_corner;
	$table_products7 = $wpdb->prefix.cupboard_additional;
	
	$sql1 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `name` text NOT NULL,
		  `photo_url` text NOT NULL,
		  `corpus_doors` text NOT NULL,
		  `corpus_height` text NOT NULL,
		  `corpus_width` text NOT NULL,
		  `corpus_depth` text NOT NULL,
		  `corpus_table_sizes` text NOT NULL,
		  `corpus_table_pricesBase` text NOT NULL,
		  `corpus_table_pricesAnother` text NOT NULL,
		  `corpus_table_articul` text NOT NULL,
		  `corpus_table_doors` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql2 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products2."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `name` text NOT NULL,
		  `photo_url` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql3 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products3."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `profile_name` text NOT NULL,
		  `profile_panels` text NOT NULL,
		  `profile_color` text NOT NULL,
		  `profile_color_url` text NOT NULL,
		  `profile_kind` text NOT NULL,
		  `profile_type` text NOT NULL,
		  `profile_type_url` text NOT NULL,
		  `profile_table_colorType` text NOT NULL,
		  `profile_table_topFrame` text NOT NULL,
		  `profile_table_bottomFrame` text NOT NULL,
		  `profile_table_divider` text NOT NULL,
		  `profile_table_topTrack` text NOT NULL,
		  `profile_table_bottomTrack` text NOT NULL,
		  `profile_table_handle` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql4 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products4."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `frame_name` text NOT NULL,
		  `frame_array` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql5 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products5."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `image_array` text NOT NULL,
		  `image_cost` text NOT NULL,
		  `image_fillMethod` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql6 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products6."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `name` text NOT NULL,
		  `photo_url` text NOT NULL,
		  `corner_width` text NOT NULL,
		  `corner_table_sizes` text NOT NULL,
		  `corner_table_pricesBase` text NOT NULL,
		  `corner_table_pricesAnother` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$sql7 =
	"
		CREATE TABLE IF NOT EXISTS `".$table_products7."` (
		  `id` int(10) NOT NULL AUTO_INCREMENT,
		  `name` text NOT NULL,
		  `photo_url` text NOT NULL,
		  `price` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
	
	$wpdb->query($sql1);
    $wpdb->query($sql2);
    $wpdb->query($sql3);
    $wpdb->query($sql4);
    $wpdb->query($sql5);
    $wpdb->query($sql6);
    $wpdb->query($sql7);
	
}

function cupboardcalc_uninstall()
{
    global $wpdb;
	
	$table_products = $wpdb->prefix.cupboard_corpuses;
	$table_products2 = $wpdb->prefix.cupboard_laminate;
	$table_products3 = $wpdb->prefix.cupboard_profile;
	$table_products4 = $wpdb->prefix.cupboard_frame;
	$table_products5 = $wpdb->prefix.cupboard_image;
	$table_products6 = $wpdb->prefix.cupboard_corner;
	
    $sql1 = "DROP TABLE `".$table_products."`;";
    $sql2 = "DROP TABLE `".$table_products2."`;";
    $sql3 = "DROP TABLE `".$table_products3."`;";
    $sql4 = "DROP TABLE `".$table_products4."`;";
    $sql5 = "DROP TABLE `".$table_products5."`;";
    $sql6 = "DROP TABLE `".$table_products6."`;";
	
    $wpdb->query($sql1);
    $wpdb->query($sql2);
	$wpdb->query($sql3);
	$wpdb->query($sql4);
	$wpdb->query($sql5);
	$wpdb->query($sql6);
}

register_activation_hook( __FILE__, 'cupboardcalc_install');
//register_deactivation_hook( __FILE__, 'cupboardcalc_uninstall');




?>