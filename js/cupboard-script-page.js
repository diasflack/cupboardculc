jQuery.noConflict();
(function( $ ) {
	$(function() {

		$(document).ready(function() {
			/*

			 БЛОК ОБЩЕЕ

			 */

//Функция проверки формы
			function array_sum(m) {
				for(var s = 0, k = m.length; k; s += parseInt(m[--k]));
				return s;
			};

			function alertForm(id, form, e) {

				if ($(id).val() == '') {
					e.preventDefault();
					$(id).addClass('alert');
					$(form).find('.form_error').text('Заполните все поля формы!');
				} else {
					$(id).removeClass('alert');
				}
			}
			function refreshScrollSpy(){
				jQuery("body").each(function(){
					jQuery(this).scrollspy("refresh");
				})
			}

			function range_onChange(input) {

				$(input).on('change', function(){

					var id = $(this).attr('id');
					var value = $(this).val();

					if ($(this).attr('class') == 'cupboard_page_corners_range') {

						$(this).siblings('.'+id+'').find('strong').text(value);

					} else {
						$(this).parents().find('.'+id+'').text(value);

						clearAll();
					}

					if ($(this).attr('id') == 'cupboard_page_width_range') {

					}

				});

			}

			function clearAll() {

				$("#corpus").slideUp();
				$(".cupboard_page_doors").slideUp();
				$('.cupboard_page_corners').slideUp();
				$('.cupboard_page_laminate').slideUp();
				$('.cupboard_page_profile').slideUp();
				$('.cupboard_page_additional').slideUp();

				$('.cupboard_page_corners_choose').slideUp();
				$('.cupboard_page_corners_main_checkbox').attr('checked',false);
				$('#cupboard_page_corners_check').attr('checked',false);
				$('.cupboard_page_corners_images').slideUp();
				$('.cupboard_page_corners_chosen label').fadeOut();

				$('#cupboard_page_additional_check').attr('checked',false);
				$('.cupboard_page_additional_images').empty();
				$('.cupboard_page_additional_choose').slideUp();

				$(".cupboard_page_laminates").slideUp();
				$(".cupboard_page_profile_list").slideUp();

				$(".cupboard_page_frame").slideUp();
				$(".cupboard_page_image").slideUp();
				$(".cupboard_page_frame_list").slideUp();
				$(".cupboard_page_image_list").slideUp();

				$(".cupboard_page_complete").slideUp();

				$('#corpus_cost').val(0);
				$('#corner_cost_left').val(0);
				$('#corner_cost_right').val(0);
				$('#profile_cost').val(0);
				$('#frame_cost').val(0);
				$('#image_cost').val(0);

				$('#cost').text(0);
			}

			function cupboard_full_cost() {
				var corpus_cost = parseFloat($('#corpus_cost').val());
				var corner_cost_left = parseFloat($('#corner_cost_left').val());
				var corner_cost_right = parseFloat($('#corner_cost_right').val());
				var profile_cost = parseFloat($('#profile_cost').val());
				var frame_cost = parseFloat($('#frame_cost').val());
				var image_cost = parseFloat($('#image_cost').val());
				var additional_cost = parseFloat($('#additional_cost').val());
				var city_cost = $('#city_cost').val() !== '0' ? parseFloat($('#city_cost').val()) : '1';

				var discount=0;

				($('#cupboard_discount_value_top').length > 0 && !$('#cupboard_discount_value_top').hasClass('inactive')) ? discount = $('#cupboard_discount_value_top').val() : '';

				var full_cost = ((corpus_cost+corner_cost_left+corner_cost_right+profile_cost+image_cost+frame_cost+additional_cost)*(1-discount/100)*city_cost).toFixed(0);

				full_cost = Math.ceil(full_cost/50)*50;

				$('#cost').text(full_cost);
			}

			$('#installment').on('change', function() {

				if ($(this).attr('checked')) {
					$('#discount_top, #discount_reason').hide();
					$('#cupboard_discount_value_top').addClass('inactive');
				} else {
					$('#discount_top, #discount_reason').show();
					$('#cupboard_discount_value_top').removeClass('inactive');
				}

				cupboard_full_cost();

			});

			$('#city_price').on('change', function() {

				if ($("#cupboard_calc_sizes").css('display') === 'none' && $(this).val() !== "" ) {
					$(this).attr("disabled","disabled");
					$("#cupboard_calc_sizes").slideDown();
				}

				$('#city_cost').val($(this).val());
				cupboard_full_cost();
			});

			$('#city_price').val('').removeAttr("disabled");

			/*

			 БЛОК ЦЕНА 305

			 */

			$(window).scroll(function() {
				if ($(this).scrollTop() > 255) {
					$('#cost_top').addClass('fixed');
					$('#cupboard_calc_page').css('margin-top','170px');
				} else {
					$('#cost_top').removeClass('fixed');
					$('#cupboard_calc_page').css('margin-top','0px');
				}
			});


			/*

			 БЛОК КОРПУСЫ

			 */

			function fillCorpuses(data) {
				var corpuses = jQuery.parseJSON(data);

				$('.cupboard_page_corpuses').empty();

				if (corpuses != null) {

					for (var i=0; i<corpuses.length; i++) {

						$('.cupboard_page_corpuses').append("<div class='corpus_div'>"+
							"<img class='corpus_image' src='"+corpuses[i]['corpus_url']+"'/>"+
							"<p class='corpus_name'>"+corpuses[i]['corpus_name']+"</p>"+
							"<input type='hidden' class='corpus_doors' value='"+corpuses[i]['corpus_doors']+"'>"+
							"<input type='hidden' class='corpus_priceBase' value='"+corpuses[i]['corpus_pricesBase']+"'>"+
							"<input type='hidden' class='corpus_priceAnother' value='"+corpuses[i]['corpus_pricesAnother']+"'>"+
							"<input type='hidden' class='corpus_articul' value='"+corpuses[i]['corpus_articul']+"'>"+
							"</div>");


					}

					$("#boxscroll").niceScroll({cursorborder:"",cursorcolor:"#ffe400",boxzoom:false, cursorwidth:"10", railoffset:"10"}); // First scrollable DIV
					$("#corpus").slideDown();
					$("#boxscroll").show();
					refreshScrollSpy();

				} else {

					$('.cupboard_page_corpuses').append("<h2 class='corpus_error'>По Вашему запросу ничего не найдено. Попробуйте выбрать другие габариты.</h2>");
					$(".cupboard_page_corpuses").slideDown();
					refreshScrollSpy();
					$("#boxscroll").getNiceScroll().resize();

				}
			}

			document.cookie = "htsadmprot=INVALID; path=/; domain=mebelite.su";

			$('#cupboard_page_search_corpuses').on('click', function() {
				var corpus_width = $('#cupboard_page_width_range').val();
				var corpus_height = $('#cupboard_page_height_range').val();
				var corpus_depth = $('#cupboard_page_depth_range').val();

				var data = {
					action: 'corpus_values',
					corpus_height: corpus_height,
					corpus_width: corpus_width,
					corpus_depth: corpus_depth
				};

				$.post(ajax_object.ajax_url, data, function(response) {
					fillCorpuses(response);
				});
			});

			$('.cupboard_page_corpuses').on('click', '.corpus_div', function() {

				$('.corpus_div').removeClass('active');
				$(this).addClass('active');

				var corpus_doors = $(this).find('.corpus_doors').val();
				corpus_doors = corpus_doors.split(',');

				$('.cupboard_page_doors .cupboard_page_doors_radio').empty();

				for (var i=0; i<corpus_doors.length; i++) {
					$('.cupboard_page_doors .cupboard_page_doors_radio').append("<option class='corpus_doors_radio' name='corpus_doors' value='"+corpus_doors[i]+"'>"+corpus_doors[i]+"</option>");
				}

				$('.cupboard_page_doors .corpus_doors_radio:first-child').attr('checked', 'checked');

				var door_number = $('.cupboard_page_doors .corpus_doors_radio:first-child').val();
				cupboard_model_draw(door_number);

				$(".cupboard_page_doors").slideDown();
				$('.cupboard_page_corners').slideDown();
				$('.cupboard_page_additional').slideDown();
				$('.cupboard_page_laminate').slideDown();


				//подсчет
				var cupboard_cost;
				var corpus_height = $('#cupboard_page_height_range').val();

				corpus_height == 2400 ? cupboard_cost = $(this).find('.corpus_priceBase').val() : cupboard_cost = $(this).find('.corpus_priceAnother').val();

				$('#corpus_cost').val(cupboard_cost);
				refreshScrollSpy();
				cupboard_full_cost();
			});

			/*

			 БЛОК Угловые сегменты

			 */


//Нажатие checkbox основного
			$('#cupboard_page_corners_check').on('change', function() {

				if ($(this).attr('checked')) {
					$('.cupboard_page_corners_choose').slideDown();
				} else {
					$('.cupboard_page_corners_choose').slideUp();
					$('.cupboard_page_corners_main_checkbox').attr('checked',false);
					$('.cupboard_page_corners_images').slideUp();
					$('.cupboard_page_corners_chosen label').fadeOut();
					$('#corner_cost_left').val('0.00');
					$('#corner_cost_right').val('0.00');
					cupboard_full_cost();
				}
				refreshScrollSpy();
			});


//Нажатие checkbox левого-правого
			$('.cupboard_page_corners_main_checkbox').on('change', function() {
				if ( $(this).attr('id') == 'cupboard_page_corners_left' ) {
					var sideConers = "left";
					$(".ml-choose-left").addClass("active");
					$(".ml-choose-right").removeClass("active");
					$("#carousel-right").css({'display':'none'});
				} else {
					var sideConers = "right";
					$(".ml-choose-right").addClass("active");
					$(".ml-choose-left").removeClass("active");
					$("#carousel-left").css({'display':'none'});
				}

				if ($(this).attr('checked')) {

					$(this).parents('label').siblings('.cupboard_page_corners_chosen').slideDown();

					var checkbox_images = $(this).parents('label').siblings('.cupboard_page_corners_chosen').find('.cupboard_page_corners_images');
					if ( checkbox_images.hasClass("selected") ) {
						return;
					}
					var data = {
						action: 'corners_values'
					};

					$.post(ajax_object.ajax_url, data, function(response) {

						var corners = jQuery.parseJSON(response);

						checkbox_images.empty();

						for (var i=0; i<corners.length; i++) {

							checkbox_images.append("<li class='selectedCorner"+sideConers+"' data-side='"+sideConers+"'>"+
								"<div class='corner_div' data-id="+i+">"+
								"<img class='corner_image' src='"+corners[i]['corner_url']+"'/>"+
								"<input type='hidden' class='corner_name' value='"+corners[i]['corner_name']+"'/>"+
								"<input type='hidden' class='corner_width' value='"+corners[i]['corner_width']+"'/>"+
								"<input type='hidden' class='corner_table_sizes' value='"+corners[i]['corner_table_sizes']+"'/>"+
								"<input type='hidden' class='corner_table_pricesBase' value='"+corners[i]['corner_table_pricesBase']+"'/>"+
								"<input type='hidden' class='corner_table_pricesAnother' value='"+corners[i]['corner_table_pricesAnother']+"'/>"+
								"</div>"+
								"<div><span>Ширина:</span><select data-side='"+sideConers+"' style='display:none;' data-id="+i+" id='cupboard_page_corner_width_range_"+sideConers+"' class='cupboard_page_corners_range ml-select'></select></div>"+
								"</li>");
							checkbox_images.addClass('selected');
							checkbox_images.slideDown();

						}

					});

				} else {
					$(this).parents('label').siblings('.cupboard_page_corners_chosen').slideUp();
					$(this).parents('label').siblings('.cupboard_page_corners_chosen').find('label').fadeOut();
					var name = $(this).attr('name');
					name === 'corners_left' ? $('#corner_cost_left').val('0.00') : $('#corner_cost_right').val('0.00');
					cupboard_full_cost();
				}
				refreshScrollSpy();
			});


//Нажатие на картинку
			$('.cupboard_page_corners').on('click', '.corner_div', function() {

				var removeSide = $(this).parent().attr('data-side');

				if ( $(this).parent().hasClass("active") ){
					$(this).parent().removeClass("active");
					$("#corner_cost_"+removeSide+"").val("0.00");
					$(this).next("div").find("select[data-side='"+removeSide+"']").css({'display':'none'});
					cupboard_full_cost();
					return;
				}

				$(".selectedCorner"+removeSide+"").removeClass('active');
				$(this).parent().addClass('active');
				var corner_width = $(this).find('.corner_width').val().split(',');
				var label = $(this).parents('div').siblings('label');
				var cornerId = $(this).attr("data-id");
				$('.cupboard_page_corners_range[data-side="'+removeSide+'"]').css({'display':'none'}).empty();
				var select = $('select[data-id="'+cornerId+'"][data-side="'+removeSide+'"]');
				$(select).css({'display':'block'});
				//$(label).find('.cupboard_page_corners_range').empty();

				for (var i=parseInt(corner_width[0]); i <= parseInt(corner_width[1]); i = i + parseInt(corner_width[2])) {
					$(select).append('<option>'+i+'</option>');
				}

				//подсчет
				var corner = this;
				cupboard_corner_cost(corner, select);
				refreshScrollSpy();
			});

			$('.cupboard_page_corners_range').on('change', function(){
				var corner = $(this).parents('label').siblings('.cupboard_page_corners_images').find('.corner_div.active');
				refreshScrollSpy()
				cupboard_corner_cost(corner);
			});

			function cupboard_corner_cost(corner, select) {
				var corner_cost;
				var corpus_height = $('#cupboard_page_height_range').val();

				var corner_table_sizes = $(corner).find('.corner_table_sizes').val().split(',');
				var corner_table_pricesBase = $(corner).find('.corner_table_pricesBase').val().split(',');
				var corner_table_pricesAnother = $(corner).find('.corner_table_pricesAnother').val().split(',');

				//var corner_range = $(corner).next('.cupboard_page_corners_range');
				var corner_current_width = select.val();

				var i = 0;

				while (corner_current_width != corner_table_sizes[i] && i < corner_table_sizes.length) {
					i++;
				}

				corpus_height == 2400 ? corner_cost = corner_table_pricesBase[i] : corner_cost = corner_table_pricesAnother[i];

				$(corner).parents('.cupboard_page_corners_left').length > 0 ? $('#corner_cost_left').val(corner_cost) : $('#corner_cost_right').val(corner_cost);
				refreshScrollSpy();
				cupboard_full_cost();
			}

			/*

			 БЛОК Дополнительные элементы

			 */


//Нажатие checkbox основного
			$('#cupboard_page_additional_check').on('change', function() {

				if ($(this).attr('checked')) {

					$('.cupboard_page_additional_choose').slideDown();

					var additional_images = $('.cupboard_page_additional_list').find('.cupboard_page_additional_images');

					var data = {
						action: 'additional_values'
					};

					$.post(ajax_object.ajax_url, data, function(response) {

						var additional = jQuery.parseJSON(response);

						for (var i=0; i<additional.length; i++) {

							additional_images.append("<li class='additional_div'>"+
								"<div class='hover-block'><div class='additional_image'><img class='addt-img' src='"+additional[i]['additional_url']+"'/></div>"+
								"<p class='additional_name'>"+additional[i]['additional_name']+"</p></div>"+
								"<div class='additional_amount_div'><label>Кол-во: <input type='number' class='additional_amount' min='1' value='1'/></label></div>"+
								"<input type='hidden' class='additional_name_input' value='"+additional[i]['additional_name']+"'/>"+
								"<input type='hidden' class='additional_price' value='"+additional[i]['additional_price']+"'/>"+
								"</li>");

							additional_images.slideDown();

						}

					});



				} else {
					$('.cupboard_page_additional_choose').slideUp();
					$('.cupboard_page_additional_images').empty();
					$('#additional_cost').val('0.00');
					cupboard_full_cost();
				}
				refreshScrollSpy();
			});

//Нажатие на картинку
			$('.cupboard_page_additional').on('click', '.hover-block', function() {

				var ad_div = $(this).parent();

				ad_div.hasClass('active') ? ad_div.removeClass('active') : ad_div.addClass('active');

				ad_div.find('.additional_amount').val(1);

				//подсчет
				refreshScrollSpy()
				cupboard_additional_cost();

			});

			$('.cupboard_page_additional').on('change', '.additional_amount', function() {
				cupboard_additional_cost();
			});

			function cupboard_additional_cost() {

				var additional_cost = 0;

				var ad_div = $('.additional_div.active');

				$.each(ad_div, function(i){

					additional_cost += parseInt($(this).find('.additional_price').val())*parseInt($(this).find('.additional_amount').val());

				});

				$('#additional_cost').val(additional_cost);

				cupboard_full_cost();
				refreshScrollSpy();
			}

			/*
			 БЛОК ЛАМИНАТ
			 */

			$('#cupboard_page_search_laminate').on('click', function() {

				var data = {
					action: 'laminate_values'
				};

				$.post(ajax_object.ajax_url, data, function(response) {

					var laminate = jQuery.parseJSON(response);

					$('.cupboard_page_laminates').empty();

					for (var i=0; i<laminate.length; i++) {

						$('.cupboard_page_laminates').append("<div class='laminate_div'>"+
							"<img class='laminate_image' src='"+laminate[i]['laminate_url']+"'/>"+
							"<p class='laminate_name'>"+laminate[i]['laminate_name']+"</p>"+
							"<input type='hidden' class='laminate_name' value='"+laminate[i]['laminate_name']+"'>"+
							"</div>");

						$(".cupboard_page_laminates").slideDown();

					}
				});
				$("#boxscroll1").niceScroll({cursorborder:"",cursorcolor:"#ffe400",boxzoom:false, cursorwidth:"10"}); // Second scrollable DIV
				refreshScrollSpy();

			});

			$('.cupboard_page_laminate').on('click', '.laminate_div', function() {

				$('.laminate_div').removeClass('active');
				$(this).addClass('active');
				$(".profile-a").addClass('block slideUp');
				$('.cupboard_page_profile').slideDown();
				refreshScrollSpy();
			});

			/*

			 БЛОК ПРОФИЛЬ

			 */

			$('#cupboard_page_search_profile').on('click', function() {
				$('.profile_div').css({'display':'block'});
				var data = {
					action: 'profile_values'
				};

				$.post(ajax_object.ajax_url, data, function(response) {

					var profile = jQuery.parseJSON(response);
					$(".ml-list-choose-left-menu").empty();
					var profile_div = '';

					for (var i=0; i<profile.length; i++) {

						profile_div += "<li class='ml-grandis1 profile_type'><label><input type='radio' name='profile_name' class='profile_name' value='"+i+"' /> "+profile[i]['profile_name']+"</label>"+
							"<input type='hidden' class='profile_panels' value='"+profile[i]['profile_panels']+"' /> "+
							"<input type='hidden' class='profile_table_colorType' value='"+profile[i]['profile_table_colorType']+"' /> "+
							"<input type='hidden' class='profile_table_topFrame' value='"+profile[i]['profile_table_topFrame']+"' /> "+
							"<input type='hidden' class='profile_table_bottomFrame' value='"+profile[i]['profile_table_bottomFrame']+"' /> "+
							"<input type='hidden' class='profile_table_divider' value='"+profile[i]['profile_table_divider']+"' /> "+
							"<input type='hidden' class='profile_table_topTrack' value='"+profile[i]['profile_table_topTrack']+"' /> "+
							"<input type='hidden' class='profile_table_bottomTrack' value='"+profile[i]['profile_table_bottomTrack']+"' /> "+
							"<input type='hidden' class='profile_table_handle' value='"+profile[i]['profile_table_handle']+"' />"+
							"<input type='hidden' class='profile_divider_cost' value='0' /></li>";

					}

					$('.ml-list-choose-left-menu').append(profile_div);

					$(".cupboard_page_profile_list").slideDown();

					profile_choice(profile);
				});
				refreshScrollSpy();
			});
			function profile_choice(profile) {
				$('.profile_name').on('change', function() {
					var z = $(this).val();
					$(".profile_type").removeClass('active');
					$(this).parent().parent().addClass('active');
					var profile_color = profile[z]['profile_color'];
					var profile_color_url = profile[z]['profile_color_url'];
					var profile_handler = profile[z]['profile_type'];
					var profile_kind = profile[z]['profile_kind'];
					var profile_type_url = profile[z]['profile_type_url'];
					var home_url = profile[z]['home_url'];

					var profile_handler_div = '<div class="profile_handler"><p class="ml-choose-h">Выберите тип ручки</p>';

					for (var i=0; i<profile_handler.length; i++) {

						profile_handler_div += '<ul id="carus4" class="scroll-img profile_handler_div"><li><label for="profile_handler_'+i+'"><img src="'+home_url+profile_type_url[i]+'" alt="'+profile_handler[i]+'" /><br/>';
						profile_handler_div += "<input type='radio' id='profile_handler_"+i+"' name='profile_handler' class='profile_handler_radio' alt='"+profile_handler[i]+"' value='"+i+"' /><span>"+profile_handler[i]+"</span></label>";
						profile_handler_div += "</li></div>";

					}

					profile_handler_div += '</div>';

					$('.cupboard_page_profile_list .profile_options').empty();

					$('.cupboard_page_profile_list .profile_options').append(profile_handler_div);

                    $('.profile_color_div, .profile_handler .profile_handler_div li').removeClass('active');
                    $('.profile_color').slideUp();

					profile_color_list(profile_color, profile_color_url, profile_kind, home_url);


				});
				refreshScrollSpy();
			}

			function profile_color_list(profile_color, profile_color_url, profile_kind, home_url) {

				$('.cupboard_page_profile_list').on('click', '.profile_handler .profile_handler_div label', function() {
					$('.profile_color').slideDown();
					$(this).parent().addClass('active');
					$('.profileColorList').html("");
					var profile_name = $(this).find('.profile_handler_radio').attr('alt');
					var profile_color_div = '';

					for (var i=0; i<profile_color.length; i++) {

						if (profile_name == profile_kind[i]) {

							profile_color_div += '<li class="profile_color_div" data-alt="'+profile_color[i]+'"><img class="profile_color_img" src="'+home_url+profile_color_url[i]+'" alt="'+profile_color[i]+'" /><p class="profile_color_name">'+profile_color[i]+'</p></li>';

						}
					}

					$('.profileColorList').append(profile_color_div);



				});

			}

			$('.cupboard_page_profile_list').on('click', '.profile_color_img', function() {
				$('.profile_color_div').removeClass('active');
				$(this).parent(".profile_color_div").addClass('active');
				$('.cupboard_page_frame').slideDown();
				cupboard_profile_cost(this);

			});




			function cupboard_profile_cost(object){

				if ($(object).is('img')) {
					var img_name = $(object).attr('alt');
					var radio_name = $('.profile_handler_radio:checked').attr('alt');
					var doors_number = $('.corpus_doors_radio:checked').val() || 1;
				} else if ($(object).hasClass('cupboard_page_doors_radio')) {
					var img_name = $('.profile_color_div.active .profile_color_img').attr('alt');
					var radio_name = $('.profile_handler_radio:checked').attr('alt');
					var doors_number = parseInt($(object).val());
				} else {
					var img_name = $('.profile_color_div.active .profile_color_img').attr('alt');
					var radio_name = object.attr('alt');
					var doors_number = $('.cupboard_page_doors_radio').val() || 1;
				}

				var corpus_width = $('#cupboard_page_width_range').val();

				var profile_table_colorType = $('.profile_name:checked').parent().siblings('.profile_table_colorType').val().split(',');
				var profile_table_topFrame =  $('.profile_name:checked').parent().siblings('.profile_table_topFrame').val().split(',');
				var profile_table_bottomFrame =  $('.profile_name:checked').parent().siblings('.profile_table_bottomFrame').val().split(',');
				var profile_table_divider =  $('.profile_name:checked').parent().siblings('.profile_table_divider').val().split(',');
				var profile_table_topTrack =  $('.profile_name:checked').parent().siblings('.profile_table_topTrack').val().split(',');
				var profile_table_bottomTrack =  $('.profile_name:checked').parent().siblings('.profile_table_bottomTrack').val().split(',');
				var profile_table_handle =  $('.profile_name:checked').parent().siblings('.profile_table_handle').val().split(',');
				var profile_panels = $('.profile_name:checked').parent().siblings('.profile_panels').val().split(',');

				var i=0;

				if (img_name !== undefined && radio_name !== undefined && profile_table_colorType !== undefined) {
					while (profile_table_colorType.length > i && profile_table_colorType[i].indexOf(img_name) ==-1  || profile_table_colorType.length > i && profile_table_colorType[i].indexOf(radio_name) == -1){
						i++
					}

					$('.profile_divider_cost').val(profile_table_divider[i]);

					var profile_cost = ((array_sum(profile_panels)+parseFloat(profile_table_handle[i]))*doors_number + (parseFloat(profile_table_topFrame[i])+parseFloat(profile_table_bottomFrame[i])+parseFloat(profile_table_topTrack[i])+parseFloat(profile_table_bottomTrack[i]))*corpus_width/1000).toFixed(2);

					$('#profile_cost').val(profile_cost);
					refreshScrollSpy();
					cupboard_full_cost();

				}
			}

			/*

			 БЛОК РАМКИ

			 */

			$('#cupboard_page_search_frame').on('click', function() {
				var data = {
					action: 'frame_values',
				};

				$.post(ajax_object.ajax_url, data, function(response) {

					var frame = jQuery.parseJSON(response);

					$('.frameList').empty();
					$(".frame_choose_div").slideDown();
					//рамки
					var frame_div = '';

					for (var i=0; i<frame.length; i++) {


						frame_div += "<li class='frame_area'>";

						for (var x=0; x < frame[i]['frame_array'].length; x++) {

							frame_div += "<div id='frame_"+frame[i]['frame_array'][x]['frame_id']+"' class='frame_div not_active'"+

								"style = '"+
								"top:"+frame[i]['frame_array'][x]['frame_top']+"px;"+
								"left:"+frame[i]['frame_array'][x]['frame_left']+"px;"+
								"background-color:"+frame[i]['frame_array'][x]['frame_color']+";"+
								"height:"+(frame[i]['frame_array'][x]['frame_height']/10)+"px;"+
								"width:"+(frame[i]['frame_array'][x]['frame_width']/10)+"px;"+
								"'"+

								" data-number='"+(x+1)+"' data-fix-w='"+frame[i]['frame_array'][x]['frame_fix_w']+"' data-fix-h='"+frame[i]['frame_array'][x]['frame_fix_h']+"' >"+
								"<div class='frame_status'></div></div>";

						}

						frame_div += "<p>"+frame[i]['frame_name']+"</p>";
						frame_div += "<input type='hidden' class='frame_area_name' value='"+frame[i]['frame_name']+"'/></li>";


					}

					//заполнения

					$('.frameList').append(frame_div);

					frame_click();

					$(".cupboard_page_frame_list").slideDown();
					$(".cupboard_page_image_list").slideDown();

					$(".cupboard_page_complete").slideDown();

					$('.cupboard_page_image').slideDown();

				});
				refreshScrollSpy()

			});

			function frame_click() {

				$(".cupboard_page_frame_list .frame_area").on('click', function(){
					$(".frame_area").removeClass('active');
					$(this).addClass('active');
					door = $('.not_active_door').first();

					$(door).html($(this).html());

					$(door).removeClass('not_active_door');

					$(door).append('<a class="delete_door_frame">Очистить рамку</a>');

					delete_door_frame();

					frame_resize(door);

					frame_cost();

					frame_image_full_cost();

				});

			}

			function delete_door_frame(){

				$('.delete_door_frame').on('click', function(){

					$(this).parent('.doors_div').empty().addClass('not_active_door');

					frame_cost();

					frame_image_full_cost();

				});

			}

			/*

			 БЛОК Заполнение

			 */

			$('.cupboard_page_doors .cupboard_page_doors_radio').on('change', function() {

				var door_number = $(this).val();
				cupboard_model_draw(door_number);

				//подсчет профили
				$('.profile_color_div.active').length > 0 ? cupboard_profile_cost(this) : '';

                frame_image_full_cost();
                frame_cost();

			});

			function cupboard_model_draw(door_number) {

				$('.cupboard_model').empty();

				var corpus_width = $('#cupboard_page_width_range').val();
				var corpus_height = $('#cupboard_page_height_range').val();

				var door_width = (corpus_width/door_number/5).toFixed(0);
				var door_height = (corpus_height/5).toFixed(0);

				var doors_div = '<div class="col-xs-12 col-md-3"><a href="#TB_inline?height=420&amp;width=750&amp;inlineId=image_popup" class="thickbox"><button id="frame_image_submit" class="tn btn-yellow" >Выбрать заполнение</button></a></div>';

				for (var i = 0; i < door_number; i++) {

					doors_div += "<div class='doors_div not_active_door'"+
						"style = 'width:"+door_width+"px; "+
						"height:"+door_height+"px;'>"+
						"</div>";
				}

				$('.cupboard_model').append(doors_div);

			}

			function frame_resize(door) {

				var door_width = $(door).outerWidth();
				var door_height = $(door).outerHeight();

				var proportion_width = door_width/80;
				var proportion_height = door_height/230;
				var proportion_width_normal = door_width/80;
				var proportion_height_normal = door_height/230;

				var proportion_width_fix = 2;
				var proportion_height_fix = 2;

				var top_correction = 0;
				var top_correction_top = 0;
				var left_correction = 0;
				var left_correction_left = 0;

				var frame_array = (door).find('.frame_div');

				var frame_height, frame_top, frame_div_height_normal, frame_div_height, frame_div_top;
				var frame_width, frame_left, frame_div_width_normal, frame_div_width, frame_div_left;

				if (frame_array.length > 0) {
					frame_array.each(function(i){

						frame_height = $(this).height();
						frame_top = parseInt($(this).css('top'));
						frame_width = $(this).width();
						frame_left = parseInt($(this).css('left'));

						if ($(this).data('fix-h') === 1) {
							frame_div_height_normal = frame_height*proportion_height_normal;
							frame_div_height = frame_height*proportion_height_fix;

							if (parseInt($(frame_array[i-1]).css('top')) > frame_top && (frame_height+frame_top) >= 200 ) {

								var edge_height = door_height - frame_div_height - parseInt($(frame_array[i-1]).css('top'));
								var edge_correction = edge_height - $(frame_array[i-1]).height();
								top_correction += edge_correction;

								$(frame_array[i-1]).height(edge_height);

							}

							frame_div_top = frame_top*proportion_height_normal+top_correction;

							parseInt($(frame_array[i+1]).css('top')) > frame_top ? top_correction += frame_div_height-frame_div_height_normal : '';
							proportion_height = (door_height-frame_div_height-frame_div_top)/(230-frame_height-frame_top);
						} else {
							frame_div_height = frame_height * proportion_height;
							frame_div_top = frame_top*proportion_height_normal+top_correction;
							top_correction !== 0 && parseInt($(frame_array[i+1]).css('top')) > frame_top ? top_correction += (frame_height*proportion_height - frame_height*proportion_height_normal) : '';
						}

						if ($(this).data('fix-w') === 1) {
							frame_div_width_normal = frame_width*proportion_width_normal;
							frame_div_width = frame_width*proportion_width_fix;

							if (parseInt($(frame_array[i-1]).css('left')) < frame_left && (frame_width+frame_left) >= 80 ) {

								var edge_width = door_width - frame_div_width - parseInt($(frame_array[i-1]).css('left'));
								var edge_correction = edge_width - $(frame_array[i-1]).width();
								left_correction += edge_correction;

								$(frame_array[i-1]).width(edge_width);

							}

							frame_div_left = frame_left*proportion_width_normal+left_correction;

							parseInt($(frame_array[i+1]).css('left')) > frame_left ? left_correction += frame_div_width-frame_div_width_normal : '';
							proportion_width = (door_width-frame_div_width-frame_div_left)/(80-frame_width-frame_left);
						} else {
							frame_div_width = frame_width * proportion_width;
							frame_div_left = frame_left*proportion_width_normal+left_correction;

						}
						//if ($(this).data('fix-w') === 1) {
						//	var frame_div_width = $(this).width()*proportion_width_fix;
						//} else {
						//	var frame_div_width = $(this).width() * proportion_width;
						//}

						var id = $(this).text();

						//var frame_div_left = parseInt($(this).css('left'))*frame_div_width/$(this).width();

						$(this).css({"width":frame_div_width, "height":frame_div_height, "left":frame_div_left, "top":frame_div_top});

					});
				}

			}



			function frame_cost() {

				var profile_divider_cost = $('.profile_divider_cost').val() || 0;
				var frames_cost = 0;

				$('.doors_div').each(function(){

					var door = this;

					var door_width = $(door).width()*5;
					var door_height = $(door).height()*5;

					var frames_width = 0;
					var frames_height = 0;

					if ($(door).find('.frame_div').length > 0) {
						$(door).find('.frame_div').each(function(){

							frames_width += $(this).width()*5;
							frames_height += $(this).height()*5;

						});

						frames_cost += parseFloat((frames_height+frames_width-door_width-door_height)/1000*parseFloat(profile_divider_cost));
					}

				});
				frames_cost = frames_cost.toFixed(2);

				$('#frame_cost').val(frames_cost);

				cupboard_full_cost();
			}


			$('.cupboard_model').on('click', '.frame_div', frame_image_choose);

			function frame_image_choose() {

				if ($('.working_image').length == 0) {

					$(this).hasClass('active_frame') ? $(this).removeClass('active_frame') : $(this).addClass('active_frame');

				}

				$('.active_frame').length ? $('#frame_image_submit').show() : $('#frame_image_submit').hide() ;

			}



			$('.cupboard_model').on('click', '#frame_image_submit', frame_image_submit);

			function frame_image_submit() {

				$('.active_frame').empty();

				var frames = $('.active_frame');

				var frame = frames[0];

				image_loading(frames);

			}

			function image_loading(frames) {

				$('.image_categories a').removeClass('active_image_category');
				$('.subcategory_imageGallery').css('display', 'none');
				$('.subsubcategory_imageGallery').css('display', 'none');
				$('.load_images').empty();

				$('.image_categories a').on('click', function(){

					var image_path = '';

					$('.image_categories a').removeClass('active_image_category');
					$(this).addClass('active_image_category');

					$('.load_images').empty();

					if ($(this).siblings('.imageGalleryURL').val() != ''){

						if ($(this).hasClass('subsubcategory_imageGallery')) {
							image_path += $(this).parent('li').siblings('.main_category_li').find('.main_category').text() + ' / ';
							image_path += $(this).parent('li').prevAll('.subcategory_li:first').find('.subcategory_imageGallery').text() + ' / ';
							image_path += $(this).text() + ' / ';
						} else if ($(this).hasClass('subcategory_imageGallery')) {
							image_path += $(this).parent('li').siblings('.main_category_li').find('.main_category').text() + ' / ';
							image_path += $(this).text() + ' / ';
						} else image_path += $(this).text() + ' / ';

						var images_url = $(this).siblings('.imageGalleryURL').val().split(',');
						var images_title = $(this).siblings('.imageGalleryCaption').val().split(',');

						var image_cost = $(this).siblings('.imageCost').val();


						var image_fillMethod = $(this).siblings('.fillMethod').val();

						var load_images = '';

						for (var i = 0; i < images_url.length; i++) {

							load_images += "<div class='image_div'>"+
								"<img class='image_img' src='"+images_url[i]+"' data-src='"+images_url[i]+"' title='"+image_path+images_title[i]+"' />"+
								"<input type='hidden' class='imageCost' value='"+image_cost+"'/>"+
								"<input type='hidden' class='fillMethod' value='"+image_fillMethod+"'/>"+
								"<p>"+images_title[i]+"</p></div>";
						}

						$('.load_images').html(load_images);

					} else if ($(this).hasClass('subcategory_imageGallery')) {
						$(this).parent('li').nextUntil('.subcategory_li').find('.subsubcategory_imageGallery').css('display', 'block');
					} else if ($(this).hasClass('main_category')) {
						$('.subcategory_imageGallery').css('display', 'none');
						$('.subsubcategory_imageGallery').css('display', 'none');
						$(this).parents('ul').find('li .subcategory_imageGallery').css('display', 'block');
					}

					image_click(frames);

				});
			}

			function image_click(frames) {
				$('.image_div').on('click', function(){

					var image_url = $(this).find('.image_img');
					var image_title = image_url.attr('title');
					var image_cost = $(this).find('.imageCost').val();
					var image_fillMethod = $(this).find('.fillMethod').val();

					tb_remove();

					$(frames).empty();

					if (image_fillMethod == '0'){

						var frame = frames[0];

						$.each(frames, function(i){
							if (parseInt($(this).css('top')) < parseInt($(frame).css('top'))){
								var temp = frame;
								frames[0] = this;
								frames[i] = temp;
							}
						});

						$(frames[0]).html('<div class="frame_image_div"><input type="hidden" class="imageCost" value="'+image_cost+'"/><input type="hidden" class="frameCost" /><input type="hidden" class="imageTitle" value="'+image_title+'" /></div>');
						$(frames[0]).find('.frame_image_div').append(image_url);

						$(frames).css('background-image','none');

						frame_resizable_image(frames);
					}

					if (image_fillMethod == '1'){

						image_url = image_url.attr('src');

						$(frames).css('background-image','url('+image_url+')');
						$(frames).html('<input type="hidden" class="imageCost" value="'+image_cost+'"/><input type="hidden" class="frameCost" /><input type="hidden" class="imageTitle" value="'+image_title+'"/><div class="frame_status empty_frame"></div>');

						$(frames).removeClass('active_frame');
						$(frames).removeClass('not_active');

						$('#frame_image_submit').hide();

						frame_image_full_cost();

					}

				});
			}

			$('.cupboard_model').on('click', '.empty_frame', empty_frame);

			function empty_frame(){

				$(this).parent().css('background-image','none');

				$(this).siblings('input').remove();

				$(this).siblings('.frame_image_div').remove();

				$(this).removeClass('empty_frame');

				$(this).parent().addClass('not_active');

				frame_image_full_cost();

			}

			function frame_resizable_image(frames) {

				var frame = frames[0];

				$(frames).parent('a').bind('click', false);

				var image_div = $(frame).find('.frame_image_div');
				var outer_frame = $(frame).find('.outer_frame');

				var frame_width = $(frame).width();
				var frame_height = $(frame).height();

				var doorOffset = $('.doors_div:first').offset();
				var mainOffset = $(frame).offset();

				var frameOffsetHeight = 0;
				var frameOffsetTop = $('.cupboard_model').height();
				var frameOffsetWidth = 0;
				var frameOffsetLeft = $('.cupboard_model').width();
				var offset = 0;

				$(frames).each(function(i){

					offset = $(this).offset();
					var height = $(this).outerHeight();
					var width = $(this).width();

					if (frameOffsetHeight < Math.abs(offset.top - mainOffset.top)+height) {
						frameOffsetHeight = Math.abs(offset.top - mainOffset.top)+height;
					}

					if (frameOffsetWidth < Math.abs(offset.left - mainOffset.left) + width) {
						frameOffsetWidth = Math.abs(offset.left - mainOffset.left) + width;
					}

					if (frameOffsetTop > offset.top - mainOffset.top) {
						frameOffsetTop = offset.top - mainOffset.top;
					}

					if (frameOffsetLeft > offset.left - mainOffset.left) {
						frameOffsetLeft = offset.left - mainOffset.left;
					}

				});

				var image = $(frame).find('.image_img');

				var width = image.width();
				var height = image.height();
				var aspectRatio = width/height;

				if (image.width() === 0) {

					image.load(function(){

						var width = image.width();
						var height = image.height();
						var aspectRatio = width/height;

						if (frameOffsetHeight>frameOffsetWidth) {
							frame_height = frameOffsetHeight;
							frame_width = frameOffsetHeight*aspectRatio;
							if (frame_width < frameOffsetWidth) {
								frame_height = frameOffsetWidth/aspectRatio;
								frame_width = frameOffsetWidth;
							}
						} else {
							frame_height = frameOffsetWidth/aspectRatio;
							frame_width = frameOffsetWidth;
							if (frame_height < frameOffsetHeight) {
								frame_height = frameOffsetHeight;
								frame_width = frameOffsetHeight*aspectRatio;
							}
						}

						image_div.css({
							'min-height': frame_height,
							'min-width': frame_width,
							'height': frame_height,
							'width': frame_width,
							'top': frameOffsetTop,
							'left': frameOffsetLeft});

						image.css({
							'min-height': frame_height,
							'min-width': frame_width,
							'height': frame_height,
							'width': frame_width});

						image.resizable({
							aspectRatio: aspectRatio,
							handles: 'n, e, s, w, ne, se, sw, nw',
							minHeight: frame_height,
							minWidth: frame_width
						});
					});

				} else {

					if (frameOffsetHeight>frameOffsetWidth) {
						frame_height = frameOffsetHeight;
						frame_width = frameOffsetHeight*aspectRatio;
						if (frame_width < frameOffsetWidth) {
							frame_height = frameOffsetWidth/aspectRatio;
							frame_width = frameOffsetWidth;
						}
					} else {
						frame_height = frameOffsetWidth/aspectRatio;
						frame_width = frameOffsetWidth;
						if (frame_height < frameOffsetHeight) {
							frame_height = frameOffsetHeight;
							frame_width = frameOffsetHeight*aspectRatio;
						}
					}

					image_div.css({
						'min-height': frame_height,
						'min-width': frame_width,
						'height': frame_height,
						'width': frame_width,
						'top': frameOffsetTop,
						'left': frameOffsetLeft});

					image.css({
						'min-height': frame_height,
						'min-width': frame_width,
						'height': frame_height,
						'width': frame_width});

					image.resizable({
						aspectRatio: aspectRatio,
						handles: 'n, e, s, w, ne, se, sw, nw',
						minHeight: frame_height,
						minWidth: frame_width
					});
				}

				image.css('border','2px dashed black');
				$('.image_overlay').show();
				$('.image_overlay_descr').show();

				image_div.addClass('working_image');
				$(frames).addClass('working_frame');
				$(frame).addClass('main_working_frame');
				image_div.draggable();

			}

			$('#rotateImage').on('click', rotateImage);

			function rotateImage() {

				var image = $('.main_working_frame').find('.working_image img');

				var imgsrc = image.data('src');

				image.addClass('image-loading');
				var data = {
					action: 'rotate_image',
					image: imgsrc,
					rotated: image.hasClass('rotate')
				};

				$.post(ajax_object.ajax_url, data, function(response) {
					image.hasClass('rotate') ? image.removeClass('rotate') : image.addClass('rotate');
					image.removeClass('image-loading');
					image.attr('src', response);
				});

			}


			$('#acceptImage').on('click', acceptImage);

			function acceptImage() {
				var frames = $('.active_frame');
				var frame = frames.find('.frame_image_div').parent()[0];

				var image = $(frame).find('.image_img');
				var image_div = $(frame).find('.frame_image_div');

				image.css('border','none');
				$('.image_overlay').hide();
				$('.image_overlay_descr').hide();

				image_div.removeClass('working_image');

				$(frames).removeClass('working_frame');
				$(frames).removeClass('active_frame');
				$(frame).removeClass('main_working_frame');
				$(frames).removeClass('not_active');

				$('#frame_image_submit').hide();

				var mainOffset = $(frame).offset();

				imagePosition = $(image_div).position();

				$(frames).each(function(){

					offset = $(this).offset();

					frameOffsetHeight = offset.top - mainOffset.top;
					frameOffsetWidth = offset.left - mainOffset.left;

					var image_div_to = image_div.clone();
					var image_to = image.clone();

					$(this).append(image_div_to);

					$(this).append('<div class="frame_status empty_frame"></div>')

					imageOffsetTop = imagePosition.top - frameOffsetHeight;
					imageOffsetLeft = imagePosition.left - frameOffsetWidth;

					var width = image_to.width();
					var height = image_to.height();

					image_div_to.resizable();
					image_div_to.resizable('destroy');

					image_div_to.draggable();
					image_div_to.draggable('destroy');

					image_div_to.find('img').width(width+6);
					image_div_to.find('img').height(height+6);

					image_div_to.find('.ui-wrapper').width(width+6);
					image_div_to.find('.ui-wrapper').height(height+6);

					$(this).find('.frame_image_div').css({'top': imageOffsetTop+ 'px', 'left': imageOffsetLeft + 'px'});

				});

				$(image_div).remove();

				$(frames).parent('a').unbind('click', false);

				frame_image_full_cost();

			}



			function frame_image_full_cost() {

				var image_full_cost = 0;

				$('.frame_div').each(function(){

					if ($(this).find('.imageCost').val() != undefined) {

						var frame_width = $(this).width()*5/1000;
						var frame_height = $(this).height()*5/1000;

						var image_cost = (frame_width*frame_height)*$(this).find('.imageCost').val();

						$(this).find('.frameCost').val(image_cost);

						image_full_cost += parseFloat($(this).find('.frameCost').val());

					}

				});

				image_full_cost.toFixed(2);

				$('#image_cost').val(image_full_cost);

				cupboard_full_cost();

			}

			/*

			 БЛОК Завершение выбора

			 */

			$('#cupboard_page_complete').on('click', function(){
				$('.corpus_div.active').length != 0 ? $('.error_corpus').css('display','none') : $('.error_corpus').css('display','block');
				$('.laminate_div.active').length != 0 ? $('.error_laminate').css('display','none') : $('.error_laminate').css('display','block');
				$('.profile_handler .profile_handler_radio:checked').length != 0 ? $('.error_profile_handler').css('display','none') : $('.error_profile_handler').css('display','block');
				$('.profile_color_div.active').length != 0 ? $('.error_profile').css('display','none') : $('.error_profile').css('display','block');
				$('.doors_div.not_active_door').length == 0 ? $('.error_frame').css('display','none') : $('.error_frame').css('display','block');
				$('.doors_div .frame_div.not_active').length == 0 ? $('.error_image').css('display','none') : $('.error_image').css('display','block');

				if ($('.corpus_div.active').length != 0 && $('.laminate_div.active').length != 0 && $('.profile_handler .profile_handler_radio:checked').length != 0 && $(".profile_color_div.active").length != 0 && $('.doors_div.not_active_door').length == 0 && $('.doors_div .frame_div.not_active').length == 0) {

					cupboard_calc_complete();

					$('#cupboard_calc').fadeOut();
					$('#cupboard_result').fadeIn();

					$('.thickbox').fadeOut();
					$('#boxscroll').getNiceScroll().hide();
					$('#boxscroll1').getNiceScroll().hide();

					$('html, body').animate({scrollTop: 0}, 1000);
				}

			});

			function cupboard_calc_complete() {

				//corpus_result
				$('#clientCity').text($('#city_price option:selected').text());

				$('.cupboard_corpus_result img').attr('src', $('.corpus_div.active .corpus_image').attr('src'));
				$('.cupboard_corpus_result img').css('width','240px');

				$('#corpus_articul_result').text($('.corpus_div.active .corpus_articul').val());
				$('#corpus_width_result').text($('#cupboard_page_width_range').val());
				$('#corpus_height_result').text($('#cupboard_page_height_range').val());
				$('#corpus_depth_result').text($('#cupboard_page_depth_range').val());
				$('#corpus_doors_result').text($('.corpus_doors_radio:checked').val());

				//corners
				if ($('#cupboard_page_corners_check').attr('checked')) {

					$('.cupboard_corners_result').css('display', 'block');

					if ($('.cupboard_page_corners_left .cupboard_page_corners_images li').hasClass('active')) {
						$('.cupboard_corners_result .left').css('display', 'block');
						$('#corner_name_left_result').text($('.cupboard_page_corners_left li.active .corner_name').val());
						$('#corner_left_result').text($('.cupboard_page_corners_left li.active .cupboard_page_corners_range').val());
					}

					if ($('.cupboard_page_corners_right .cupboard_page_corners_images li').hasClass('active')) {
						$('.cupboard_corners_result .right').css('display', 'block');
						$('#corner_name_right_result').text($('.cupboard_page_corners_right li.active .corner_name').val());
						$('#corner_right_result').text($('.cupboard_page_corners_right li.active .cupboard_page_corners_range').val());
					}

				}

				//additional
				if ($('#cupboard_page_additional_check').attr('checked')) {

					$('.cupboard_additional_result').css('display', 'block');

					var additional_result = '';
					var ad_div = $('.additional_div.active');

					$.each(ad_div, function(i){

						additional_result += "<p>"+$(this).find('.additional_name_input').val()+" - "+$(this).find('.additional_amount').val()+"</p>";

					});

					$('.cupboard_additional_result').html(additional_result);

				}

				//laminate
				$('#laminate_result').text($('.laminate_div.active .laminate_name').text());

				//profile
				$('#profile_type_result').text($('.profile_name:checked').parent().text());
				$('#profile_color_result').text($(".profile_color_div.active").attr('data-alt'));
				$('#profile_handle_result').text($('.profile_handler_radio:checked').parent().text());

				//frame

				var frames = $('.cupboard_model_result');

				$('.frame_status').hide();

				$('.cupboard_model').find('.doors_div').each(function(x) {

					var frame_div = $('<div class="frame_result_div"></div>').appendTo(frames);

					$('.cupboard_model .frame_div').each(function(){
						$(this).append("<span class='frame_number'>"+$(this).data('number')+"</span>");
					});

					var self = this;

					html2canvas(self, {
						onrendered: function(canvas) {
							frame_div.append(canvas);

							var door_model = $(self).find('.frame_area_name').val();
							var div = '<div class="descr"><p>Дверь №'+(x+1)+'</p><p>Модель двери - '+door_model+'</p>';
							$(self).find('.frame_div').each(function(i) {
								var image = $(this).find('.imageTitle').val();
								div += '<p>Вставка №'+(i+1)+' - '+image+'</p>';
							});
							div += '</div>';
							frame_div.append(div);
						}
					});

				});

				//cost

				$('#cost_result').text($('#cost').text());
				$('#manager_cost_result').text($('#cost').text());

				if ($('#installment').attr('checked') == 'checked') { $('#installment_result').text('Да') } else { $('#installment_result').text('Нет') }

			}

			$('#save').click(function(e){
				e.preventDefault();

				var canvas = $("canvas"), canvases = [];

				$.each(canvas, function(i){
					canvases.push($(this)[0].toDataURL())
				});

				var data = {
					action: 'send_mail',
					frames: canvases
				};

				$.post(ajax_object.ajax_url, data, function(response) {

					console.log(response);

				});
			});

			$('#cupboard_send').on('click', function(e){
				e.preventDefault();

				$('#clientName').val() === '' ? $('#clientNameError').show() : $('#clientNameError').hide();
				$('#clientTel').val() === '' ? $('#clientTelError').show() : $('#clientTelError').hide();

				if ($('#clientName').val() !== '' && $('#clientTel').val() !== '') {

					var cupboard_result = $('#cupboard_result').clone();

					cupboard_result.find('#clientName').replaceWith($('#clientName').val());
					cupboard_result.find('#clientTel').replaceWith($('#clientTel').val());

					cupboard_result.find('.clear').remove();
					cupboard_result.find('input').remove();
					cupboard_result.find('button').remove();

					cupboard_result = cupboard_result[0].outerHTML;

					//door_frames
					var canvas = $("canvas"), canvases = [];

					$.each(canvas, function(i){
						canvases.push($(this)[0].toDataURL())
					});

					var data = {
						action: 'send_mail',
						cupboard_result: cupboard_result,
						frames: canvases
					};

					$.post(ajax_object.ajax_url, data, function(response) {

						$('html, body').animate({scrollTop: 0}, 1000);

						$('#cupboard_result').fadeOut();

						$('#cupboard_calc_page .success').fadeIn();
						$('.thickbox').fadeIn();

						$("#test").removeClass("affix");
						$('#ascrail2000, #ascrail2001').show();

						$('.cupboard_model_result').empty();
						$('.frame_status').show();
						$('.frame_number').remove();

						setTimeout("jQuery('#cupboard_calc_page .success').fadeOut();", 2000);
						setTimeout("jQuery('#cupboard_calc').fadeTo('slow',1);", 2000);

					});

				} else {
					$('html, body').animate({scrollTop: 0}, 1000);

				}

			});

			$('#cupboard_print').on('click', function(e){
				e.preventDefault();

				window.print();
			});

			$('#cupboard_back').on('click', function(e){
				e.preventDefault();

				$('.cupboard_model_result').empty();
				$('.frame_status').show();
				$('.frame_number').remove();

				$('#cupboard_result').fadeOut();
				$('#cupboard_calc').fadeIn();


				$('.thickbox').fadeIn();
				$('#boxscroll').getNiceScroll().resize().show();
				$('#boxscroll1').getNiceScroll().resize().show();
			});

			/*

			 БЛОК Разное

			 */

			range_onChange('#cupboard_page_height_range');
			range_onChange('#cupboard_page_width_range');
			range_onChange('#cupboard_page_depth_range');
			range_onChange('#cupboard_page_corner_width_range_left');
			range_onChange('#cupboard_page_corner_width_range_right');

		});
	});
})(jQuery);

