jQuery.noConflict();
(function( $ ) {
  $(function() {
  
$(document).ready(function() {

/*

	БЛОК Все
	
*/

$(".show_list").on("click", show_list);

function show_list(event){
    var $this = $(this);
    $this.parents(".list_table").toggleClass("visible");
    $this.parents(".list_table").hasClass("visible") ? $this.val("Скрыть") : $this.val("Показать");
}


/*

	БЛОК ОБЩЕЕ
	
*/

//Функция проверки формы	
function alertForm(id, form, e) {
	
	if ($(id).val() == '') {
			e.preventDefault();
			$(id).addClass('alert');
			$(form).find('.form_error').text('Заполните все поля формы!');
		} else {
			$(id).removeClass('alert');
		}
}	

//Функция оптравки формы на добавление
	$('.add_form').on('submit', function(e) {
		
		var form = this;
	
		$(form).find('input').each(function(){
			if ($(this).attr('type') != 'file' && $(this).attr('type') != 'hidden') {
				alertForm(this, form, e);
			}
		});
		
		if ($(form).find('.corpus_add_input_sizes').length == 0 && $(form).attr('id') == 'corpus_add') {
			e.preventDefault();
			$(form).find('.form_table_error').text('Обновите таблицу цен!');
		} else $(form).find('.form_table_error').text('');
		
		if ($(form).find('.corner_add_input_sizes').length == 0 && $(form).attr('id') == 'corner_add') {
			e.preventDefault();
			$(form).find('.form_table_error').text('Обновите таблицу цен!');
		} else $(form).find('.form_table_error').text('');

	});
	
//Функция оптравки формы на редактирование
	$('.list_form').on('submit', function(e) {
	
		var form = this;
	
		$(form).find('input').each(function(){
			if ($(this).attr('type') != 'file' && $(this).attr('type') != 'hidden') {
				alertForm(this, form, e);
			}
		});
		
		if ($(form).find('.corpus_add_input_sizes').length == 0 && $(form).attr('id') == 'corpus_list') {
			e.preventDefault();
			$(form).find('.form_table_error').text('Обновите таблицу цен!');
		} else $(form).find('.form_table_error').text('');

	});

/*

	БЛОК НАСТРОЙКА
	
*/

	$('.button_city_add').on('click', function(e) {
		e.preventDefault();
		$(this).parents("tr").next("tr").after("<tr>"+
							"<td></td>"+
							"<td style='text-align:right;'><label>Название:</label></td>"+
							"<td>"+
								"<input type='text' style='width: 200px' name='cupboard_cities_city[]' value=''/>"+
								" <label>Множитель:</label>"+
								" <input type='text' style='width: 100px' name='cupboard_cities_multi[]' value=''/>"+
								" <a class='button_city_delete'>X</a>"+
							"</td>"+
						"</tr>");
	});
	
	$('.base_form').on('click','.button_city_delete', function(e) {
		e.preventDefault();
		$(this).parents("tr").remove();
	});

/*

	БЛОК КОРПУС
	
*/

//Обновление таблицы цен - КОРПУС
	$('.corpus_add_size_table_reload').click(function(e) {
		e.preventDefault();
		
		var form = $(this).closest('form');
		
		var corpus_add_max_width = parseInt($(form).find('#corpus_add_max_width').val()),
			corpus_add_min_width = parseInt($(form).find('#corpus_add_min_width').val()),
			corpus_add_step_width = parseInt($(form).find('#corpus_add_step_width').val()),
			corpus_add_table_size,sizes_range = [],sizes = [];
		
		if (corpus_add_max_width > 0 && corpus_add_min_width > 0 && corpus_add_step_width > 0) {
			corpus_add_table_size = Math.floor((corpus_add_max_width - corpus_add_min_width)/corpus_add_step_width+1);
			
			for(var i=corpus_add_min_width-corpus_add_step_width;sizes_range.push(i+=corpus_add_step_width)<corpus_add_table_size;);
			
			$(form).find('.corpus_add_input_sizes').each(function(){
				var value = parseInt($(this).val());
				if (value > corpus_add_max_width || value < corpus_add_min_width || $.inArray(value, sizes_range) == -1) {
						var index = $(this).parent().index()+1;
						$(form).find('.corpus_add_size_table tr').find("td:nth-child("+index+")").remove();
					} else {
						sizes.push(value);
					}
					});
			
			for(var i=0; i <= corpus_add_table_size; i++) {
				
				var size = parseInt(corpus_add_min_width)+i*corpus_add_step_width;
				
				if (size > parseInt(corpus_add_max_width) || $.inArray(size, sizes) != -1) continue;
				
				$(form).find('.corpus_add_size_table .corpus_add_raw_sizes').append("<td><input type='text' class='corpus_add_input_sizes' name='corpus_table_sizes[]' style='width:50px;' value='"+size+"' /></td>");
				$(form).find('.corpus_add_size_table .corpus_add_raw_pricesBase').append("<td><input type='text' class='corpus_add_input_pricesBase' name='corpus_table_pricesBase[]' style='width:50px;' value='' /></td>");
				$(form).find('.corpus_add_size_table .corpus_add_raw_pricesAnother').append("<td><input type='text' class='corpus_add_input_pricesAnother' name='corpus_table_pricesAnother[]' style='width:50px;' value='' /></td>");
				$(form).find('.corpus_add_size_table .corpus_add_raw_articul').append("<td><input type='text' class='corpus_add_input_articul' name='corpus_table_articul[]' style='width:50px;' value='' /></td>");
				$(form).find('.corpus_add_size_table .corpus_add_raw_doors').append("<td><label> 2 <input type='checkbox' class='corpus_table_doors' value='2'/></label><br>"+
																					"<label> 3 <input type='checkbox' class='corpus_table_doors' value='3' /></label><br>"+
																					"<label> 4 <input type='checkbox' class='corpus_table_doors' value='4' /></label>"+
																					"<input type='hidden' name='corpus_table_doors[]' /></td>");
					
			}	
		}	
			
	});

//Нажатие checkbox основного
$('.corpus_add_size_table .corpus_add_raw_doors').on('change', '.corpus_table_doors', function() {
	var values = [],
		$this = $(this),
		$input = $(this).parent().siblings('input');

	values = $input.val() ? $input.val().split(",") : [];

	if ($this.attr('checked')) {
		values.push($this.val());
	} else {
		var index = values.indexOf($this.val());
		if (index > -1) {
			values.splice(index, 1);
		}
	}
	$input.val(values);
});
	
/*

	БЛОК ПРОФИЛЬ
	
*/

//Обновление таблицы цен - ПРОФИЛЬ
	$('.profile_add_colorType_table_reload').click(function(e) {
		e.preventDefault();
		
		var form = $(this).closest('form');
		
		var profile_color_array = [], 
			profile_color_names = [],
			names = [];
		
		$(form).find('.profile_color').each(function(i) {
			profile_color_array[i]=[];
			profile_color_array[i]['color'] = $(this).val();
			profile_color_array[i]['kind'] = $(this).siblings('.profile_kind').val();
			profile_color_names[i] = profile_color_array[i]['kind']+" + "+profile_color_array[i]['color'];
		});
		
		if (profile_color_array.length > 0) {
			
			$(form).find('.profile_add_input_colorType').each(function(){
				var value = $(this).val();
				if ($.inArray(value, profile_color_names) == -1) {
					var index = $(this).parent().index()+1;
					$(form).find('.profile_add_colorType_table tr').find("td:nth-child("+index+")").remove();
				} else {
					names.push(value);
				}
				
			});
			
			for(var x=0; x < profile_color_array.length; x++) {
				
				var name = profile_color_array[x]['kind']+" + "+profile_color_array[x]['color'];
				
				if ($.inArray(name, names) != -1) continue;
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_colorType').append("<td><p>"+profile_color_array[x]['kind']+" + "+profile_color_array[x]['color']+"</p>"+
					"<input type='hidden' class='profile_add_input_colorType' name='profile_table_colorType[]' style='width:50px;' value='"+profile_color_array[x]['kind']+" + "+profile_color_array[x]['color']+"' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_topFrame').append("<td><input type='text' class='profile_add_input_topFrame' name='profile_table_topFrame[]' style='width:50px;' value='' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_bottomFrame').append("<td><input type='text' class='profile_add_input_bottomFrame' name='profile_table_bottomFrame[]' style='width:50px;' value='' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_divider').append("<td><input type='text' class='profile_add_input_divider' name='profile_table_divider[]' style='width:50px;' value='' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_topTrack').append("<td><input type='text' class='profile_add_input_topTrack' name='profile_table_topTrack[]' style='width:50px;' value='' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_bottomTrack').append("<td><input type='text' class='profile_add_input_bottomTrack' name='profile_table_bottomTrack[]' style='width:50px;' value='' /></td>");
					
					$(form).find('.profile_add_colorType_table .profile_add_raw_handle').append("<td><input type='text' class='profile_add_input_handle' name='profile_table_handle[]' style='width:50px;' value='' /></td>");
				
			}
		}
	});
	
//Добавление варианта цвета - ПРОФИЛЬ
$('.profile_add_color').click(function(e){
	e.preventDefault();
	
	var options = $(this).closest('form').find('.profile_kind').html();
	
	$(this).parents('tr').after("<tr class='profile_color_raw'><td></td><td>Название цвета: <input type='text' class='profile_color' name='profile_color[]' accept='image/*' style='width:150px;'/> Вид профиля: <select type='text' class='profile_kind' name='profile_kind[]' style='width:150px;'>"+options+" </select> Фото: <input type='file' name='profile_foto[]' accept='image/*' style='width:300px;'/><input type='hidden' class='profile_input_color_url' name='profile_input_color_url[]' style='width:50px;' value='0' /><a class='profile_delete_color'>X</a></td></tr>");
	$(this).siblings('.profile_filelength').val(parseInt($(this).siblings('.profile_filelength').val()) + 1);
});

$('form').on('click', '.profile_color_raw td a.profile_delete_color', function(){
	var filelength = $(this).closest('form').find('.profile_color_raw').length-1;
	
	$(this).parents('form').find('.profile_filelength').val(filelength);

	$(this).parents('tr').remove();
});

//Добавление типа - ПРОФИЛЬ
$('.profile_add_type').click(function(e){
	e.preventDefault();
	
	var filelength = $(this).closest('form').find('.profile_type_raw').length+1;
	
	$(this).parents('tr').after("<tr class='profile_type_raw'><td></td><td>Название: <input type='text' class='profile_type' name='profile_type[]' for='profile_kind_"+filelength+"' accept='image/*' style='width:150px;'/> Фото: <input type='file' name='profile_type_foto[]' accept='image/*' style='width:300px;'/><input type='hidden' class='profile_input_type_url' name='profile_input_type_url[]' style='width:50px;' value='0' /> <a class='profile_delete_type'> X </a></td></tr>");
	
	$(this).closest('form').find('.profile_kind').append('<option class="profile_kind_'+filelength+'"></option>');
	
	$(this).siblings('.profile_type_filelength').val(filelength);
});

$('form').on('click', '.profile_type_raw td a.profile_delete_type', function(){	
	var filelength = $(this).closest('form').find('.profile_type_raw').length-1;
	
	$(this).parents('form').find('.profile_type_filelength').val(filelength);
	
	var for_kind = $(this).siblings('.profile_type').attr('for');
	
	$(this).closest('form').find('.'+for_kind).remove();
	
	$(this).parents('tr').remove();	
	
});

$('body').on('input', '.profile_type', function(){
	
	var profile_type = $(this).val();
	var for_kind = $(this).attr('for');
	
	$('.'+for_kind).val(profile_type)
					.text(profile_type);
	
});

/*

	БЛОК РАМКИ
	
*/

var frame_id = 0;
var proportion = $(".frame_proportion").val();

//Пропорции - РАМКИ

$(".frame_proportion").change(function() {

	var form = $(this).closest('form');

	var old_proportion = $(form).find(".frame_old_proportion").val();
	proportion = $(form).find(".frame_proportion").val();
	var frame_width = 800 / proportion;
	var frame_height = 2300 / proportion;
	
	$(form).find('.frame_add_area').css({"width":frame_width, "height":frame_height});
	
	if ($(form).find('.frame_div').length > 0) {
		$(form).find('.frame_div').each(function(){ 
			
			var id = $(this).text();
			
			var frame_div_width = $(form).find('#frame_param_'+id).find('.frame_width').val() / proportion;
			var frame_div_height = $(form).find('#frame_param_'+id).find('.frame_height').val() / proportion;
			var frame_div_left = parseInt($(this).css('left'))*old_proportion/proportion;
			var frame_div_top = parseInt($(this).css('top'))*old_proportion/proportion;

			
			$(this).css({"width":frame_div_width, "height":frame_div_height, "left":frame_div_left, "top":frame_div_top});
				
		});
	}
	
	$(form).find(".frame_old_proportion").val(proportion);
	
});

//Добавление рамки - РАМКИ
$('.frame_add_doorFrame').click(function(e){
	e.preventDefault();
	
	var form = $(this).closest('form');
	
	$('.frame_param').length > 0 ? frame_id = parseInt($(form).find('.frame_param:last').attr('id').replace(/frame_param_/g, '')) : '';
	
	frame_id += 1;
	
	$(form).find('.frame_add_area').append('<div id="frame_'+frame_id+'" class="frame_div">'+frame_id+'</div>');
	
	var frame_width = parseInt($("#frame_"+frame_id).css("width"))*proportion;
	var frame_height = parseInt($("#frame_"+frame_id).css("height"))*proportion;
	var frame_color = '#'+'0123456789abcdef'.split('').map(function(v,i,a){
  return i>5 ? null : a[Math.floor(Math.random()*16)] }).join('');

	
	$("#frame_"+frame_id).css("background-color", frame_color);
	
	$(this).parents('.frame_add_list').append("<div id='frame_param_"+frame_id+"' class='right frame_param'>"+
	"Рамка: "+frame_id+
	" Ширина: <input type='text' class='frame_width' name='frame_width[]' style='width:50px;' value='"+frame_width+"'/> мм; "+ 
	"Высота: <input type='text' class='frame_height' name='frame_height[]' style='width:50px;' value='"+frame_height+"'/> мм; "+
	"Верт. фикс: <input type='checkbox'  name='frame_fix_h[]' data-for='frame_fix_h_hidden' value='1' /> "+
	"<input class='frame_fix_h_hidden' type='hidden' value='0' name='frame_fix_h[]'>"+
	"Гор. фикс: <input type='checkbox' name='frame_fix_w[]' data-for='frame_fix_w_hidden' value='1' />"+
	"<input class='frame_fix_w_hidden' type='hidden' value='0' name='frame_fix_w[]'>"+
	"<input type='hidden' class='frame_id' name='frame_id[]' value='"+frame_id+"'/>"+
	"<input type='hidden' name='frame_color[]' value='"+frame_color+"'/>"+
	"<input type='hidden' class='frame_top' name='frame_top[]' value=''/>"+
	"<input type='hidden' class='frame_left' name='frame_left[]' value=''/>"+
	"<a class='frame_delete_doorFrame'> X </a></div>");
	
	frame_drag_resize();
	
});

//Драг и ресайз рамок - РАМКИ

function frame_drag_resize() {
	$(".frame_div")
		.draggable({
		containment: "parent"
	//	snap: ".frame_div", snapMode: "outer"
	})
		.resizable({
			
			resize: function() {
				
				var form = $(this).closest('form');
				var frame_id = $(this).text();

				var frame_width =  parseInt($(form).find("#frame_"+frame_id).css("width"))*proportion;
				var frame_height = parseInt($(form).find("#frame_"+frame_id).css("height"))*proportion;
				
				$(form).find('#frame_param_'+frame_id+' .frame_width').val(frame_width);
				$(form).find('#frame_param_'+frame_id+' .frame_height').val(frame_height);
				
			},
			
			containment: "parent"
		
		});
}

//checkbox
$('.frame_add_list').on('click', 'input[type=checkbox]', function(e){
	
	if ($(this).attr('checked') == 'checked') { 
	
		$(this).siblings('.'+$(this).data('for')).attr('disabled','disabled');
			
	} else { 
		
		$(this).siblings('.'+$(this).data('for')).removeAttr('disabled');
		
		
	}
	
	
	
	
});

frame_drag_resize();

//Удаление рамки - РАМКИ

$('body').on('click', 'a.frame_delete_doorFrame', function(){
	var form = $(this).closest('form');
	
	var frame_id_chosen = $(this).siblings('.frame_id').val();

	
	$(form).find('#frame_'+frame_id_chosen).remove();
	
	$(this).parent('div.frame_param').remove();
});

//Ресайз рамки через инпут - РАМКИ
$(".frame_add_list").on('change', '.frame_width', function() {

	var form = $(this).closest('form');
	var id = $(this).parent().attr('id').replace(/frame_param_/g, '');

	var frame_width = $(this).val() / proportion;
	
	$(form).find('#frame_'+id).css({"width":frame_width});
	
});

$(".frame_add_list").on('change', '.frame_height', function() {

	var form = $(this).closest('form');
	var id = $(this).parent().attr('id').replace(/frame_param_/g, '');
	
	var frame_height = $(this).val() / proportion;
	
	$(form).find('#frame_'+id).css({"height":frame_height});
	
});

//отправка формы - добавление рамок
$('.frame_form').on('submit', function(e) {
	
	var form = this;
	var id, frame_top, frame_left;
	
	$(this).find('.frame_div').each(function(){
		
		id = $(this).attr('id').replace(/frame_/g, '');
		
		frame_top = parseInt($(this).css("top"))*proportion/10;
		frame_left = parseInt($(this).css("left"))*proportion/10;
		
		$(form).find('#frame_param_'+id+' .frame_top').val(frame_top);
		$(form).find('#frame_param_'+id+' .frame_left').val(frame_left);
		
	});

	$(form).find('input').each(function(){
		alertForm(this, form, e);
	});
	
	

});

/*

	БЛОК ГАЛЕРЕЯ
	
*/

//Media Manager - ГАЛЕРЕЯ
wp.media.imageGallery = {
	
	frame: function($el) {

		this._frame = wp.media({
			id:			'my-frame',				   
			frame:     	'post',
			state:     	'gallery-edit',
			title:     	wp.media.view.l10n.editGalleryTitle,
			editing:   	true,
			multiple:  	true,
		});

		var controller = this._frame.states.get('gallery-edit');

		// Turn off sortable
		controller.set('sortable', false);

		// Turn off refreshContent callback so we do not throw a null error on 
		//   frame.router.get()
		controller.get('selection').on( 'remove reset', function() {
			var controller = wp.media.imageGallery._frame.states.get('gallery-edit');
			this.off('remove reset', controller.refreshContent, controller);
		});

		// Don't display gallery settings
		controller.frame.on( 'content:create', function() {
			var controller = wp.media.imageGallery._frame.states.get('gallery-edit');

			controller.frame.off( 'content:render:browse', controller.gallerySettings, controller );
		});
		
		this._frame.on('open',function() {
			var controller = wp.media.imageGallery._frame.states.get('gallery-edit');
			var selection = controller.get('library');
			
			ids = $($el).siblings('.imageGalleryIDs').val().split(',');
			
			if (ids != '') {
				
			ids.forEach(function(id) {
				attachment = wp.media.attachment(id);
				attachment.fetch();
				selection.add( attachment ? [ attachment ] : [] );
			});
			
			}
		});
											 
		this._frame.on( 'update', 
					   function() {
			var controller = wp.media.imageGallery._frame.states.get('gallery-edit');
			var library = controller.get('library');
			
			var IDs = library.pluck('id');
			var url = library.pluck('url');
			var caption = library.pluck('title');
				
				$($el).siblings('input.imageGalleryURL').val(url);
				$($el).siblings('input.imageGalleryCaption').val(caption);
				$($el).siblings('input.imageGalleryIDs').val(IDs);
				
			// send ids to server
			wp.media.post( 'shiba-mlib-gallery-update', {
				nonce:  	wp.media.view.settings.post.nonce, 
				html:   	wp.media.imageGallery.link,
				post_id: 	wp.media.view.settings.post.id,
				ids: 		IDs
			}).done( function() {
				window.location = wp.media.imageGallery.link;
			});

			// Turn off refreshContent callback so we do not throw a null error 
			controller.off('reset'); 
			
		});

		return this._frame;
	},

	init: function() {
		$('body').on('click', '.category_imageGallery .upload-and-attach-link', function( event ) {
			var $el = $(this);
			wp.media.imageGallery.link = $el.data('updateLink');
			event.preventDefault();
			
			wp.media.imageGallery.frame($el).open();

		});
		
	}
	
	
};

//Добавление категорий и субкатегорий - ГАЛЕРЕЯ
$('body').on('click','.category_imageGallery .add_subcategory_imageGallery', function(e){
	e.preventDefault();
	
	if ($(this).text() == ' Добавить подкатегорию') {
		$(this).prev('.upload-and-attach-link').css('display', 'none');
		$(this).siblings('.imageGalleryURL').val('');
		$(this).siblings('.imageGalleryCaption').val('');
		$(this).siblings('.imageGalleryIDs').val('');
	} else if ($(this).text() == 'Добавить субподкатегорию') {
		$(this).siblings('.upload-and-attach-link').css('display', 'none');
		$(this).siblings('.imageGalleryURL').val('');
		$(this).siblings('.imageGalleryCaption').val('');
		$(this).siblings('.imageGalleryIDs').val('');
	}
	
	
	
	var divHTML = "<div class='category_imageGallery {category_subcategory}'>"+
							"<input class='{category_imageGallery}' type='text' name='imageGalleryName[]' placeholder='Введите название'/>"+
							"<input class='{category_imageGallery}' type='text' name='imageCost[]' placeholder='Спец цена' value='0' />"+
							"<a class='upload-and-attach-link' href='#'> "+
							"<img src='"+pluginURL.plugin_dir+"/css/edit.png' id='wp_editgallery' width='24' height='24' title='Редактировать галерею'> </a> {category_button} "+
							"<input type='hidden' name='imageGalleryURL[]' class='imageGalleryURL' />"+
							"<input type='hidden' name='imageGalleryCaption[]' class='imageGalleryCaption' />"+
							"<input type='hidden' name='imageGalleryIDs[]' class='imageGalleryIDs' />"+
							"<input type='hidden' name='imageGallerySub[]' class='imageGallerySub' value='{category_subcategory2}' />"+
							"<a class='imageGallery_delete_category'> X </a>"
							"</div>";
							
	if (!$(this).siblings().hasClass('subcategory_imageGallery')) {
		var category_imageGallery = 'subcategory_imageGallery';
		//var category_name = 'Подкатегория';
		var button = "<button class='add_subcategory_imageGallery'>Добавить субподкатегорию</button>";
		var category_subcategory = 'subcategory';
	} else {
		var category_imageGallery = 'subsubcategory_imageGallery';
		//var category_name = 'Суб-подкатегория';
		var button = '';
		var category_subcategory = 'subsubcategory';
	}
							
	divHTML = divHTML.replace('{category_imageGallery}', category_imageGallery)
					// .replace('{category_name}', category_name)
					 .replace('{category_subcategory}', category_subcategory)
					 .replace('{category_subcategory2}', category_subcategory)
					 .replace('{category_button}', button);
	
	$(this).parent().append(divHTML);

	
}); 

//Удаление категории - ГАЛЕРЕЯ
$('body').on('click', 'a.imageGallery_delete_category', function(){
	
	
	var el_class=$(this).siblings('.imageGallerySub').val();
	
	if ($(this).parent('.category_imageGallery').siblings("."+el_class).length == 0) {
		$(this).parent('.category_imageGallery').parent('.category_imageGallery').find('.upload-and-attach-link').css('display', 'inline-block');
	} else if ($(this).text() == 'Добавить субподкатегорию') {
		$(this).prev('.upload-and-attach-link').css('display', 'none');
	}
	
	$(this).parent('.category_imageGallery').remove();
	
});

$( wp.media.imageGallery.init );

/*

	БЛОК УГЛОВЫЕ СЕГМЕНТЫ
	
*/

//Обновление таблицы цен - УГЛОВЫЕ СЕГМЕНТЫ
$('.corner_add_size_table_reload').click(function(e) {
		e.preventDefault();
		
		var form = $(this).closest('form');
		
		var corner_add_max_width = parseInt($(form).find('#corner_add_max_width').val()),
			corner_add_min_width = parseInt($(form).find('#corner_add_min_width').val()),
			corner_add_step_width = parseInt($(form).find('#corner_add_step_width').val()),
			corner_add_table_size, sizes = [], sizes_range = [];
			
		if (corner_add_max_width > 0 && corner_add_min_width > 0 && corner_add_step_width > 0) {
			corner_add_table_size = Math.floor((corner_add_max_width - corner_add_min_width)/corner_add_step_width+1);
			
			for(var i=corner_add_min_width-corner_add_step_width;sizes_range.push(i+=corner_add_step_width)<corner_add_table_size;);
			
			$(form).find('.corner_add_input_sizes').each(function(){
				var value = parseInt($(this).val());
				if (value > corner_add_max_width || value < corner_add_min_width || $.inArray(value, sizes_range) == -1) {
						var index = $(this).parent().index()+1;
						$(form).find('.corner_add_size_table tr').find("td:nth-child("+index+")").remove();
					} else {sizes.push(value);}});
			
			for(var i=0; i <= corner_add_table_size; i++) {
			
				var size = parseInt(corner_add_min_width)+i*corner_add_step_width;
				
				if (size > parseInt(corner_add_max_width) || $.inArray(size, sizes) != -1) continue;
				
				$(form).find('.corner_add_size_table .corner_add_raw_sizes').append("<td><input type='text' class='corner_add_input_sizes' name='corner_table_sizes[]' style='width:50px;' value='"+size+"' /></td>");
				$(form).find('.corner_add_size_table .corner_add_raw_pricesBase').append("<td><input type='text' class='corner_add_input_pricesBase' name='corner_table_pricesBase[]' style='width:50px;' value='' /></td>");
				$(form).find('.corner_add_size_table .corner_add_raw_pricesAnother').append("<td><input type='text' class='corner_add_input_pricesAnother' name='corner_table_pricesAnother[]' style='width:50px;' value='' /></td>");
				$(form).find('.corner_add_size_table .corner_add_raw_articul').append("<td><input type='text' class='corner_add_input_articul' name='corner_table_articul[]' style='width:50px;' value='' /></td>");
					
			}	
		}	
			
	});

});
    
  });
})(jQuery);